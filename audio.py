
import pysrt
import json
from glob import glob
from pydub import AudioSegment
import urllib2 as urlreq # Python 2.x
import requests
from time import sleep
from googletrans import Translator
import subtitle
from cloud_client import snippets
import sys
import Queue
import threading
import time
from StringIO import StringIO
import os.path
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

reload(sys)
#sys.setdefaultencoding('utf8')

#rootAudio = None
SPEED_LEVEL_0 = 4.5
SPEED_LEVEL_1 = 4.7
SPEED_LEVEL_2 = 5.0
SPEED_LEVEL_3 = 6.66
totalSentenes = 0

currentIndexAPIKey = 0
listAPIKey = ['9c022ce0971d45fe83f6f08203730b96','5251ab16e0594e7785fc45291a591e13', '2d5ea9047063495e81063d02558dd497','f3ecc48eb1d5483b8940608848fa06f2','25fcecceca204e31b685aa9b61100005','a306852724c043c09ddf762f864e0651','7d582f1b962646cc8d6acf79976da207','d73688842c694884b39b09f4cacf225b', '7021d1d5ee3946edae2e52ea35d905d8', '0c39d74211f042c2ae615a157038421c', 'd8e7c2afa54a4785af68ed2a4aae269b', '74964de7fe634c079486ffc749da1fba']
dict_pronunciations = {}
with open('pronunciation.txt') as f:
    pronunciations = f.readlines()
    for pronunciation in pronunciations:
        arr = pronunciation.split("-")
        if len(arr) > 1:
            key = arr[0]
            value = arr[1]
            dict_pronunciations[key] = value


class AudioSubTitle:
    dict_audio = {}
    # threadList = []
    # nameList = ["One", "Two", "Three", "Four", "Five"]
    queueLock = threading.Lock()
    workQueue = Queue.Queue(80)
    threads = []
    countProcess = 0
    exitFlag = 0

    def textToSpeechViettel(self, video_id, strText, timeSecondStart, timeSecondEnd):
        url = "https://vtcc.ai/voice/api/tts/v1/rest/syn"
        data = {"text": strText, "voice": "doanngocle", "id": "2", "without_filter": False, "speed": 1.0,
        "tts_return_option": 3, "timeout": 60000, "addition_params": [{ "key": "", "value": ""}]}
        headers = {'Content-type': 'application/json', 'token': '-Jnc-FcIgsPqlADrwXzOe5ylXCL4-grV2hppTCJqsokFUl5cgcAZr2T5-A-xcxPS'}
        cert_path =  'vtcc-cert/wwwvtccai.crt'
        response = requests.post(url, data=json.dumps(data), headers=headers, verify=False)
        print(response.headers)

        # Get status_code.
        print(response.status_code)
        self.dict_audio[str(timeSecondStart)] = response.content

    def textToSpeech(self, video_id, strText, timeSecondStart, timeSecondEnd):
        global currentIndexAPIKey
        if len (strText) < 5:
            return
        speed_leave_request = SPEED_LEVEL_0
        listWords = strText.split()
        countPoints = strText.count('.')
        countComma = strText.count(',')
        countWordOfOneSecond = len(listWords)/ (((timeSecondEnd - timeSecondStart) - (countPoints * 0.66) - (countComma * 0.33)) + 1)

        if countWordOfOneSecond <= SPEED_LEVEL_0:
            speed_leave_request = 0
        elif countWordOfOneSecond <= SPEED_LEVEL_1:
            speed_leave_request = 1
        elif countWordOfOneSecond <= SPEED_LEVEL_2:
            speed_leave_request = 2
        else:
            speed_leave_request = 3
        
        # (url, access_token, api_token) = self.get_api_conf()
        api_url = 'http://api.openfpt.vn/text2speech/v4'
        data = strText.strip()

        # print(data)

        request = urlreq.Request(api_url)
        request.data = data
        # print('speed_leave_request: {}'.format(speed_leave_request))
        method = ("POST", "GET")
        request.get_method = lambda: method[0]
        request.add_header('api_key', listAPIKey[currentIndexAPIKey])
        request.add_header('voice', 'hatieumai')
        request.add_header('speed', str(speed_leave_request))
    #    print(request.data)
        d = None
        try:
            dataRes = urlreq.urlopen(request)
            response = dataRes.read()
            d = json.loads(response)
            # print(d)

        except Exception, e:
    #        if e.code == 400:
    #            return
            
            sleep(1)
            if currentIndexAPIKey < (len(listAPIKey) - 1):
                currentIndexAPIKey = currentIndexAPIKey + 1
            else:
                currentIndexAPIKey = 0
            self.textToSpeech(video_id, strText, timeSecondStart, timeSecondEnd)
            return
        path = 'data_processing/'+ video_id+'_' + str(timeSecondStart)+'.mp3'
        # print(path)
        sleep(1)
        self.download_file(video_id, d['async'], path, timeSecondStart, timeSecondEnd)

    def download_file(self, video_id, url , target, timeSecondStart, timeSecondEnd):
        try:
            src = urlreq.urlopen(url)
            data = src.read()
            
            # dst = open(target, "wb")
            self.dict_audio[str(timeSecondStart)] = data
            # dst.write(data)
            if src:
                src.close()
            # if dst:
            #     dst.close()
    #        overlayAudio(video_id, timeSecondStart, timeSecondEnd)
        except :
            sleep(1)
            self.download_file(video_id, url, target, timeSecondStart, timeSecondEnd)

    def overlayAudio(self, rootAudio,video_id, timeSeconds):
        
    #    global rootAudio
        audio = ""
        self.countProcess +=  1
        if(rootAudio == None):
            rootAudio = AudioSegment.silent(duration=(timeSeconds *1000))
        # if os.path.isfile('data_processing/'+ video_id+'_' + str(timeSeconds)+'.mp3'):
        #     # audio = AudioSegment.from_mp3('data_processing/'+ video_id+'_' + str(timeSeconds)+'.mp3')
        if str(timeSeconds) not in self.dict_audio:
            return {'rootAudio': rootAudio, 'lenAudio': 0}
        try:
            audio = AudioSegment.from_mp3(StringIO(self.dict_audio[str(timeSeconds)]))
            audio = audio[300:(audio.duration_seconds - 0.15)*1000]
            if rootAudio.duration_seconds < timeSeconds:
                rootAudio = rootAudio + AudioSegment.silent(duration=((timeSeconds -rootAudio.duration_seconds) *1000))
            #    oldAudio = audio = AudioSegment.from_mp3('test'+'.mp3')

            rootAudio =  rootAudio + audio   
        except expression as identifier:
            print('error---')

        return {'rootAudio': rootAudio, 'lenAudio': len(audio)}
        # return {'rootAudio': rootAudio, 'lenAudio': 0}
    #    rootAudio = rootAudio.overlay(oldAudio, 0)
    #    rootAudio = rootAudio.overlay(audio, timeSeconds *1000)
    #    out_f = open("test.mp3" , 'wb')
    #    rootAudio.export(out_f, format='mp3')

    def overlayListAudio(self, rootAudio, video_id, listTimes):
        prevTime = None
        listTimeAds = []
        for timeStart in listTimes:
            if prevTime != None and (timeStart *1000 - prevTime) > 7000:
                if len(listTimeAds) == 0 or (timeStart *1000 - listTimeAds[len(listTimeAds) - 1]) > 300 *1000:  
                    listTimeAds.append(len(rootAudio))        
                    # rootAudio = overlayAdsAudio(rootAudio)
            dict_output = self.overlayAudio(rootAudio, video_id, timeStart)
            rootAudio = dict_output['rootAudio']
            lenAudio = dict_output['lenAudio']
            prevTime = timeStart * 1000 + lenAudio
        return {'rootAudio': rootAudio, 'listTimeAds': listTimeAds}

    def overlayAdsAudio(self, rootAudio):
        rootAudio = rootAudio + AudioSegment.silent(duration=(1000))
        audio = AudioSegment.from_mp3('thuyetminhyoutube.mp3')
        rootAudio =  rootAudio + audio
        return rootAudio
        
    def cal_subtitle_groups(video_id, sub_path):
        subs = pysrt.open(sub_path)
        lastestTime = 0
        strOutput = ''
        fistTimeSecondOfGroup = None
        global countProcess
        global totalSentenes
        totalSentenes = 0
        countProcess = 0
        for sub in subs:
            if fistTimeSecondOfGroup == None:
                fistTimeSecondOfGroup = sub.start.seconds + (sub.start.minutes * 60)
            textLine = sub.text
            if ((sub.start.seconds + (sub.start.minutes * 60))- lastestTime) > 0.5:
                # print(fistTimeSecondOfGroup)
                # print(strOutput)
                totalSentenes = totalSentenes + 1
                self.textToSpeech(video_id, strOutput, fistTimeSecondOfGroup, lastestTime)
                strOutput = ''
                fistTimeSecondOfGroup = sub.start.seconds + (sub.start.minutes * 60)
            strOutput = strOutput + textLine
            lastestTime = sub.end.seconds + (sub.end.minutes * 60)
    #    print('totalSentenes {} countProcess {}'.format(str(totalSentenes), str(countProcess)) )
        path_audio = 'data/audio/'+video_id +'_vi.mp3'
        out_f = open(path_audio , 'wb')
        rootAudio.export(out_f, format='mp3')
        snippets.upload_blob('thuyetminh_youtube',path_audio, path_audio)
        url_audio = snippets.make_blob_public('thuyetminh_youtube', path_audio)
        return url_audio

    def replace_pronunciation(self, text):
        for key, value in dict_pronunciations.iteritems():
            text = text.replace(key,value)
            text = text.replace(key.capitalize(),value)
            text = text.replace(key.title(),value)
        return text
    def convertToAudio(self,video_id, sub_path, name_file):
        global is_progressing

        print('Starting convert to audio: ',video_id)
        while(is_progressing):
            time.sleep(3)
        
        is_progressing = True
        subs = pysrt.open(sub_path)
        lastestTime = 0
        strOutput = ''
        fistTimeSecondOfGroup = None

        self.queueLock = threading.Lock()
        self.workQueue = Queue.Queue(80)
        self.threads = []
        threadID = 1

        listTimeStart = []
        listTimeFinish = []
        totalSentenes = 0
        self.countProcess = 0
        self.queueLock.acquire()
        indexSub = 0
        rootAudio = None
        lastest_fistTime = 0
        for sub in subs:
    #        if fistTimeSecondOfGroup == None:
    #            fistTimeSecondOfGroup = sub.start.seconds + (sub.start.minutes * 60)
            textLine = sub.text.encode('utf-8')
            textLine = self.replace_pronunciation(textLine)

            fistTime = sub.start.seconds + (sub.start.minutes * 60)
            if lastest_fistTime == fistTime:
                fistTime += 1
            lastestTime = sub.end.seconds + (sub.end.minutes * 60)
            listTimeStart.append(fistTime)
            thread = myThread(self, threadID,video_id, textLine,fistTime, lastestTime,self.workQueue)
            thread.start()
            self.threads.append(thread)
            threadID += 1
            self.workQueue.put(str(fistTime))
            indexSub += 1
            lastest_fistTime = fistTime
            if threadID > 80 or indexSub == len(subs):
                self.queueLock.release()
                while not self.workQueue.empty():
                    if len(listTimeFinish) > 0:

                        dict_output = self.overlayListAudio(rootAudio,video_id, listTimeFinish)
                        rootAudio = dict_output['rootAudio']
                        listTimeFinish = []
                    pass
                # Notify threads it's time to exit
                self.exitFlag = 1

                # Wait for all threads to complete
                listTimeFinish = listTimeStart
                listTimeStart = []
                print('Wait for all threads to complete')
                for t in self.threads:
                    t.join()
                threadID = 1
                self.queueLock.acquire()
                self.threads = []
                self.exitFlag = 0

    #        textToSpeech(video_id, textLine, fistTimeSecondOfGroup, lastestTime)

        print "Exiting Main Thread"
        is_progressing = False
        # print('len audio')
        # print(len(rootAudio))
        # print(listTimeFinish)
        dict_output = self.overlayListAudio(rootAudio,video_id, listTimeFinish)
        rootAudio = dict_output['rootAudio']
        listTimeAds = dict_output['listTimeAds']
        url_audio_show = None
        # if len(listTimeAds) > 0:
        #     audioForShow = self.audio_for_show(rootAudio, listTimeAds)
        #     path_audio_show = 'data/audio/'+name_file +'_vi.mp3'
        #     out_f = open(path_audio_show , 'wb')
        #     audioForShow.export(out_f, format='mp3')
        #     snippets.upload_blob('thuyetminh_youtube', path_audio_show, path_audio_show)
        #     url_audio_show = snippets.make_blob_public('thuyetminh_youtube', path_audio_show)
        #     audioForShow = None

        print('totalSentenes {} countProcess {}'.format(str(totalSentenes), str(countProcess)) )
        path_audio_original = 'data/audio/'+name_file +'_vi_original.mp3'
        out_f = open(path_audio_original , 'wb')
        rootAudio.export(out_f, format='mp3')
        snippets.upload_blob('thuyetminh_youtube', path_audio_original, path_audio_original)
        url_audio_original = snippets.make_blob_public('thuyetminh_youtube', path_audio_original)
        rootAudio = None
        if url_audio_show == None:
            url_audio_show = url_audio_original
        print('Finish video: ', video_id)
        return {'url_audio_original':url_audio_original, 'url_audio_show': url_audio_original, 'listTimeAds':listTimeAds}

    def audio_for_show(self,audioForShow, listTimeAds):
        for time in listTimeAds:
            audio = AudioSegment.from_mp3('thuyetminhyoutube.mp3')
            audioForShow  = audioForShow.overlay(audio, time + 1000)
        return audioForShow

    def process_data(self,video_id, textLine, firstTime, lastestTime, q):
        while not self.exitFlag:
            self.queueLock.acquire()
            if not self.workQueue.empty():
                data = q.get()
                self.queueLock.release()
                self.textToSpeech(video_id, textLine, firstTime, lastestTime)
                # print "%s processing %s" % (textLine, data)
            else:
                self.queueLock.release()
            time.sleep(1)



is_progressing = False

class myThread (threading.Thread):
   def __init__(self, audioSubtitle,  threadID, video_id, textLine, firstTime, lastestTime, q):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.audioSubtitle = audioSubtitle
      self.video_id = video_id
      self.textLine = textLine
      self.firstTime = firstTime
      self.lastestTime = lastestTime
      self.q = q
   def run(self):
#      print "Starting theard" + str(threadID)
    #   print(self.firstTime)
      self.audioSubtitle.process_data(self.video_id, self.textLine, self.firstTime, self.lastestTime,  self.q)
#      print "Exiting " + str(threadID)






# if __name__ == '__main__':
#     convertToAudio('MotG3XI2qSs', '../data/sub/vi/MotG3XI2qSs_vi.srt')
