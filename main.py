#!/usr/bin/env python
from flask import request,jsonify
from flask import Flask, abort
import audio
from audio import AudioSubTitle
import subtitle
import os
import datetime
import urllib2 as urlreq # Python 2.x
from cloud_client import snippets
from waitress import serve
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "cer.json"

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello, World!"

@app.route('/api/configs')
def getConfig():
    return jsonify({'data': {'is_show_ads':True, 'version':1.0}}), 201

@app.route('/api/test_audio')
def test_audio():
    audio.convertToAudio('MotG3XI2qSs', '../data/sub/vi/MotG3XI2qSs_vi.srt')
    return "Hello, World!"

@app.route('/api/progress_subtitle_test', methods=['POST'])
def progress_video_test():
    return jsonify({'results': {'path_sub_en':'https://storage.googleapis.com/thuyetminh/data/sub/en/UF8uR6Z6KLc_en.srt', 'path_sub_vi':'https://storage.googleapis.com/thuyetminh/data/sub/vi/UF8uR6Z6KLc_vi.srt', 'path_audio': 'https://storage.googleapis.com/thuyetminh/data/audio/UF8uR6Z6KLc_vi.mp3', 'path_audio_show': 'https://storage.googleapis.com/thuyetminh/data/audio/UF8uR6Z6KLc_vi.mp3'}}), 201

@app.route('/api/progress_subtitle', methods=['POST'])
def progress_video():
    if not request.json or not 'id_video' in request.json:
        abort(400)
    id_video = request.json['id_video']
    is_sub = 0
    if request.json.get('is_sub'):
        is_sub = request.json['is_sub']

    dict_caption = None
    if os.path.isfile('pre_data/'+id_video +'.srt'):
        print('pre_data/'+id_video +'.srt')
        dict_caption = {'isTranslatable':False, 'srtPath':'pre_data/'+id_video +'.srt'}
    else:
        dict_caption = subtitle.getSubtitle(id_video)
    if dict_caption == None:
        
        dict_caption = subtitle.download_audio(id_video)
        print('not found subtitle: ',id_video )
        # abort(404)
    name_file = id_video + '-{date:%Y-%m-%d-%H-%M-%S}'.format( date=datetime.datetime.now() )
    dict_out = None
    if dict_caption['isTranslatable'] == False:
        dict_out = subtitle.translate_to_en_vi(id_video, dict_caption['srtPath'], name_file)
    else:
        dict_out = subtitle.process_subtitle_to_en_vi(id_video, dict_caption['srtPath'], name_file)
    url_sub_en = dict_out['url_sub_en']
    url_sub_vi = dict_out['url_sub_vi']
    path_sub_vi = dict_out['path_sub_vi']
    url_audio = ''
    if is_sub != 1 :
        audioSubtitle = AudioSubTitle()
        dict_output = audioSubtitle.convertToAudio(id_video, path_sub_vi, name_file)
        url_audio_original = dict_output['url_audio_original']
        url_audio_show= dict_output['url_audio_show']
        listTimeAds = dict_output['listTimeAds']
    return jsonify({'results': {'path_sub_en':url_sub_en, 'path_sub_vi':url_sub_vi, 'path_audio_show': url_audio_show, 'path_audio':url_audio_original, 'list_time_ads':listTimeAds}}), 201


def download_file(path, target):
    try:
        src = urlreq.urlopen(path)
        data = src.read()
        dst = open(target, "wb")
        dst.write(data)
        if src:
            src.close()
        if dst:
            dst.close()
#        overlayAudio(video_id, timeSecondStart, timeSecondEnd)
    except :
        print('error')
    finally:
        print('done')

@app.route('/api/subtitle_update', methods = [ 'POST'])
def upload_file():
    if 'id_video' in request.json:
        id_video = request.json['id_video']
        url_sub = request.json['url_sub']
        name_file = id_video + '-{date:%Y-%m-%d-%H-%M-%S}'.format( date=datetime.datetime.now() )
        path_save = 'data_uploaded/'+name_file+'.srt'
        download_file(url_sub,path_save)
        snippets.upload_blob('thuyetminh_youtube', path_save, 'data/sub/vi/'+ name_file+'.srt')
        url_sub_vi = snippets.make_blob_public('thuyetminh_youtube', 'data/sub/vi/'+ name_file+'.srt')
        audioSubtitle = AudioSubTitle()
        dict_output = audioSubtitle.convertToAudio(id_video, path_save,name_file)
        url_audio_original = dict_output['url_audio_original']
        url_audio_show= dict_output['url_audio_show']
        listTimeAds = dict_output['listTimeAds']
        return jsonify({'results': {'path_sub_vi':url_sub_vi, 'path_audio_show': url_audio_show, 'path_audio':url_audio_original,'list_time_ads':listTimeAds}}), 201
    abort(404) 

@app.route('/api/subtitle_update_test', methods = [ 'POST'])
def upload_file_test():
    return jsonify({'results': {'path_sub_vi':'https://storage.googleapis.com/thuyetminh_youtube/data/sub/vi/C5pKtnmHTxg-2019-02-24-06-59-54_vi.srt', 'path_audio': 'https://storage.googleapis.com/thuyetminh_youtube/data/audio/C5pKtnmHTxg-2019-02-24-09-51-13_vi.mp3', 'path_audio_original':'https://storage.googleapis.com/thuyetminh_youtube/data/audio/C5pKtnmHTxg-2019-02-24-09-51-13_vi_original.mp3','list_time_ads':[]}}), 201
    abort(404) 

      
if __name__ == '__main__':
    # app.run(host='0.0.0.0', port=8888, debug=True)
    serve(app, host='0.0.0.0', port=8888)
