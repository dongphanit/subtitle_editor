#!/usr/bin/env python
import os
import datetime
import urllib2 as urlreq # Python 2.x
import json
import subtitle
import time


def getPlaylist(channelId):
    url = 'https://www.googleapis.com/youtube/v3/playlists?part=snippet&channelId='+ channelId +'&key=AIzaSyC0IyRI9fyBxCSheNHlaocz3kSlXhIRrZE'
    print(url)
    try:
        dataRes = urlreq.urlopen(url)
        print(dataRes)
        response =  dataRes.read()
        print(response)
        # print(response)
        d = json.loads(response)
        return d
    except  :
        print('Error')
        return None
        
def getListVideosID(playListId):
    url = 'https://www.googleapis.com/youtube/v3/playlistItems?playlistId='+playListId+'&key=AIzaSyC0IyRI9fyBxCSheNHlaocz3kSlXhIRrZE&part=snippet&maxResults=50'
    print(url)
    try:
        dataRes = urlreq.urlopen(url)
        response =  dataRes.read()
        d = json.loads(response)
        return d
    except  :
        print('Error')
        return None

def getSubByChanelId(channelId):
    resPlaylist = getPlaylist(channelId)
    print(resPlaylist['items'])
    for item in resPlaylist['items']:
        resVideos = getListVideosID(item['id'])
        for item in resVideos['items']:
            id_video = item['snippet']['resourceId']['videoId']
            print(id_video)
            dict_caption_out = subtitle.getSubtitle(id_video)
            if dict_caption_out != None:
                name_file = id_video + '-{date:%Y-%m-%d-%H-%M-%S}'.format( date=datetime.datetime.now() )
                if dict_caption_out['isTranslatable'] == False:
                    subtitle.translate_to_en_vi(id_video, dict_caption_out['srtPath'], name_file, False)
                else:
                    subtitle.process_subtitle_to_en_vi(id_video, dict_caption_out['srtPath'], name_file, False)
            # print(item['snippet']['resourceId']['videoId'])
            time.sleep( 20 )

if __name__ == '__main__':
    with open('channel.txt') as f:
        channels = f.readlines()
        for x in channels:
            print('----- START CHANNEL: '+x.strip()+'------')
            getSubByChanelId(x.strip())
            print('------ END CHANNEL-------')
        

    


