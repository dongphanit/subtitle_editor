#!/usr/bin/env python
import sys
import os
import datetime as dt
import glob
from bs4 import BeautifulSoup

def xml2srt(pathname):
    root, _ = os.path.splitext(pathname)
    if os.path.exists('%s.srt' % root):
        return

    with open(pathname) as fin:
        text = fin.read()
        text = text.replace('-1:', '00:') # broken xml
        soup = BeautifulSoup(text,'html.parser')
    ps = soup.findAll('p')
    print(ps)

    with open('%s.srt' % root, 'w') as fout:
        for i, p in enumerate(ps):
            begin = p['begin']
            timestamp, seconds = begin[:5], begin[6:] # begin.rsplit(':', 1)
            begin = dt.datetime.strptime(timestamp, '%H:%M') + \
                    dt.timedelta(0, 0, float(seconds) * 1e6)

            dur = p['dur']
            timestamp, seconds = dur[:5], dur[6:] # dur.rsplit(':', 1)
            dur = dt.datetime.strptime(timestamp, '%H:%M') + \
                  dt.timedelta(0, 0, float(seconds) * 1e6)
            dur = dt.timedelta(0, dur.hour,
                    (dur.minute*60 + dur.second) * 1e6 + dur.microsecond)
            end = begin + dur

            begin = '%s,%03d' % (begin.strftime('%H:%M:%S'), begin.microsecond//1000)
            end   = '%s,%03d' % (end.strftime('%H:%M:%S'), end.microsecond//1000)

            print(p.text)

if __name__ == '__main__':
    
    xml2srt('/Users/dongphan/Downloads/timedtext.xml')
