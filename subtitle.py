import json
from urllib2 import urlopen
from urllib import urlencode
from urlparse import parse_qs, urlparse
import youtube2srt
import pysrt
from googletrans import Translator
# from google.cloud import translate
from google.cloud import translate_v3beta1 as translate
from punctuator import play_with_model 
from bs4 import BeautifulSoup
from google.cloud import storage
from cloud_client import snippets
import os
import sys
import re
import youtube_dl
from autosub import generate_subtitles

reload(sys)  
sys.setdefaultencoding('utf8')


os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "cer.json"
dict_terminology = {}
with open('terminology.txt') as f:
    terminologys = f.readlines()
    index = 0
    for terminology in terminologys:
        dict_terminology['XXXXXXX{}'.format(str(index))] = terminology
        index = index + 1


def download_audio(video_id):
    ydl_opts = {
        'format': 'bestaudio/best',
        'outtmpl':'%(id)s.%(ext)s',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '64',
        }]
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download(['https://www.youtube.com/watch?v='+video_id])
    
    srtPath = 'data/sub/orginal/'+video_id +'_orginal.srt'
    generate_subtitles(video_id+'.mp3', output= srtPath,  api_key='AIzaSyC0IyRI9fyBxCSheNHlaocz3kSlXhIRrZE')
    os.remove(video_id+'.mp3')
    return {'isTranslatable':False, 'srtPath':srtPath, 'languageCode': 'en'}
    
    


def getSubtitle(video_id):
    srtPath = 'data/sub/orginal/'+video_id +'_orginal.srt'
    output = youtube2srt.youTube2Srt("https://video.google.com/timedtext?type=track&v="+video_id+"&id=0&lang=en", srtPath)
    isTranslatable = False
    captionsurl = ''
    languageCode = 'en'
    if output == None:
        isTranslatable = True
        vidinfo = urlopen('https://www.youtube.com/get_video_info?&video_id='+ video_id).read().decode('utf8')
        parsed = parse_qs(vidinfo)
        if 'player_response' not in parsed:
            return None
        parsedjson = json.loads(parsed['player_response'][0])
        try:
            listCaptions = parsedjson['captions']['playerCaptionsTracklistRenderer']['captionTracks']
            # print(listCaptions)
            captionsurl = ''
            if len(listCaptions) == 1:
                item  = listCaptions[0]
                captionsurl = item['baseUrl']
                isTranslatable = False
                languageCode = item['vssId'].replace('a.','')


            for item in listCaptions:
                if item['languageCode'] == 'en' :
                    if len(captionsurl) == 0 or 'kind' not in item or item['kind'] != 'asr':
                        captionsurl = item['baseUrl']
                        if 'kind' not in item or item['kind'] != 'asr':
                            isTranslatable = False
                    # break
                    
        except:
            print("Caption not found", video_id)
            
        if(len(captionsurl) == 0):
            return None # Caption not found
#    captionsurl = parsedjson['captions']['playerCaptionsTracklistRenderer']['captionTracks'][0]['baseUrl']
        print(captionsurl)
        output = youtube2srt.youTube2Srt(captionsurl, srtPath)
    
    if output == None:
        return None
    return  {'isTranslatable':isTranslatable, 'srtPath':srtPath, 'languageCode': languageCode}
#    captionsinfo = urllib.request.urlopen(captionsurl).read()
#    soup = BeautifulSoup(captionsinfo, 'html.parser')

#    print(soup.contents)


def convertToFinalSentene(strSentence):
    # listChar = [',COMMA', ':COLON', '?QUESTIONMARK', '-DASH', '!EXCLAMATIONMARK ', '!EXCLAMATIONMARK ', ';SEMICOLON'  ]
    strSentence = strSentence.replace(',COMMA', ',')
    strSentence = strSentence.replace(':COLON', ':')
    strSentence = strSentence.replace('?QUESTIONMARK', '?')
    strSentence = strSentence.replace('-DASH', '-')
    strSentence = strSentence.replace('!EXCLAMATIONMARK ', '!')
    strSentence = strSentence.replace(';SEMICOLON', ';')
    strSentence = regexSpecialCharater(strSentence)
    return strSentence

def removeSpecialCharater(strSentence):
    strSentence = strSentence.replace('&#39', "'s")
    strSentence = strSentence.replace('&quot;', '"')
    strSentence = strSentence.replace('/', ' trên ')
    return strSentence

def getText(lst_text):
    out = ''
    for text in lst_text:
        out += ' ' + text
    return out

def getTextTrans(lst_text):
    out = ''
    for item in lst_text:
        out += ' ' + item.text
    return out

def translate_to_en_vi(video_id, orginal_path, name_file, translate_vi = True):
    subs = pysrt.open(orginal_path)
    path_sub_en = 'data/sub/en/'+name_file +'_en.srt'
    path_sub_vi = 'data/sub/vi/'+name_file +'_vi.srt'
    # subs.save( path_sub_en, encoding='utf-8')
    snippets.upload_blob('thuyetminh_youtube', orginal_path, path_sub_en)
    url_sub_en = snippets.make_blob_public('thuyetminh_youtube', path_sub_en)
    if translate_vi:
        translator = Translator(service_urls=[
            'translate.google.com',
            'translate.google.co.kr',
        ])
        list_text = []
        translations = []
        startTime = 0
        sentence = ''
        index = 0
        
        while(index < len(subs) ):
            sub = subs[index]
            nextSub = None
            if (index + 1) < len(subs):                
                nextSub = subs[index + 1]
                
            if '.' in sub.text or (nextSub and sub.end.seconds != nextSub.start.seconds):
                # sub.position = str(index) 
                sentence += ' '+ sub.text
                sentence = preSubtext(sentence)
                sentence = regexSpecialCharater(sentence)
                sentence = sentence.replace('>>', '').replace('&gt;&gt','')
                sentence = encode_terminology(sentence)
                sub.text = sentence
                if startTime > 0:
                    sub.start.minutes = 0
                    sub.start.seconds = startTime
                if len(sentence) > 0:
                    list_text.append(sentence)
                    index += 1
                else:
                    subs.remove(sub)
                startTime = 0
                sentence = ''
            else:
                if startTime == 0:
                    startTime =  sub.start.seconds + (sub.start.minutes * 60)
                sentence += ' ' + sub.text
                subs.remove(sub)

               
        for i in range(int(len(list_text)/20) + 1):
            try:
                # text_translate = getText(list_text[i*20: (i *20 +20)])
                # out_trans = translator.translate(text_translate, dest='vi')
                # print('out_trans',out_trans.text.decode('utf-8'))
                
                translation  = translator.translate(list_text[i*20: (i *20 +20)], dest='vi')
                # print('translate', getTextTrans(translation).decode('utf-8'))
                translations += translation
            except :
                translations = []
                break
        is_free = True
        if len(translations) == 0:
            is_free = False
            translations = []
            client = translate.TranslationServiceClient()
            location = 'global'
            parent = client.location_path('fresh-aura-230118', location)
            for i in range(int(len(list_text)/20) + 1):
                response = client.translate_text(
                    parent=parent,
                    contents=list_text[i*20: (i *20 +20)],
                    mime_type='text/plain',  # mime types: text/plain, text/html
                    # source_language_code='en-US',
                    target_language_code='vi-VN')
                translations += response.translations
             
        # print(translations)
        for i in range(len(translations)):
            item = translations[i]
            strTranslator = ''
            if(is_free):
                strTranslator = item.text
            else:
                strTranslator = item.translated_text
           
            strTranslator = decode_terminology(strTranslator)
            text = removeSpecialCharater(strTranslator)
            if len(text) == 0:
                subs[i].text  = ' '
            else:
                subs[i].text = text

        subs.save(path_sub_vi, encoding='utf-8')
        snippets.upload_blob('thuyetminh_youtube', path_sub_vi, path_sub_vi)
        url_sub_vi = snippets.make_blob_public('thuyetminh_youtube', path_sub_vi)
        return {'url_sub_en': url_sub_en, 'url_sub_vi': url_sub_vi, 'path_sub_vi':  path_sub_vi}
    else:
        subs.save('pre_data/'+video_id+'.srt', encoding='utf-8')
   
def regexSpecialCharater(value):
    try:
        pattern = re.compile(r'[\(]+[^,\)]+[\)]')  # Example: (sadsadsa)
        pattern1 = re.compile(r'[\[]+[^,\]]+[\]]')  # Example: [adsadsa]
        list_value = re.findall(pattern, value)
        list_value += re.findall(pattern1, value)
        for item in list_value:
            value = value.replace(item, '')
        return value
    except re.error:
        return value
    
def preSubtext(text):
    lstChar = ['<font color="#E5E5E5">', '<font color="#CCCCCC">', '<font color="#CCCCCC">', '<font color="#FEFEFE">', '</ font>', '</font>', '>>']
    for item in lstChar:
        text = text.replace(item, '')
    text = re.sub(r"<>", "", text)
    return text


def process_subtitle_to_en_vi(video_id, orginal_path, name_file, translate_vi = True):
    subs = pysrt.open(orginal_path)
    lastestTime = 0
    strOutput = ''
    fistTimeSecondOfGroup = None
    global countProcess
    global totalSentenes
    totalSentenes = 0
    countProcess = 0
    for sub in subs:
        sub.text = preSubtext(sub.text)
        strOutput = strOutput + ' '+sub.text
    # strOutput = regexSpecialCharater(strOutput)
    currentIndexSub = 0
    strOutput = play_with_model.play(strOutput)

    timeSecondStartSub = None
    sentenes = strOutput.split('.PERIOD')
    for sentene in sentenes:
        # Remove Excess data
        sentenePre = sentene.replace(',COMMA ', '')
        sentenePre = sentenePre.replace(':COLON ', '')
        sentenePre = sentenePre.replace('?QUESTIONMARK ', '')
        sentenePre = sentenePre.replace('-DASH ', '')
        sentenePre = sentenePre.replace('!EXCLAMATIONMARK ', '')
        sentenePre = sentenePre.replace(';SEMICOLON ', '')
        sentenePre = sentenePre.strip()

        # sentenePre :du lieu da xoa cac dau co ban: , :, ?, -, ;
        # delNextSubtitle: du lieu con lai de xu ly tiep trong 1 cau
        delNextSubtitle = sentenePre
        doneSentene = False
        while not doneSentene:
            if currentIndexSub >= (len(subs) - 1):
                doneSentene = True
                break
            sub = subs[currentIndexSub]
            if timeSecondStartSub == None:
                timeSecondStartSub = sub.start.seconds + (sub.start.minutes * 60)
                
                # print(timeSecondStartSub)
            sub.text = sub.text.strip()
            if len(convertToFinalSentene(sub.text)) == 0:
                subs.remove(sub)
                timeSecondStartSub = None

            # Truong hop 1 sub nho hon 1 cau
            elif sub.text in delNextSubtitle and len(sub.text) != len(delNextSubtitle):
                delNextSubtitle = delNextSubtitle.replace(sub.text, '', 1)
                delNextSubtitle = delNextSubtitle.strip()
                delNextSubtitleFixed = delNextSubtitle.strip()
                
                nextSub = subs[currentIndexSub + 1]
#                print('-----------')
                if nextSub.text == delNextSubtitleFixed:
                    subs.remove(sub)
#                    currentIndexSub = currentIndexSub + 1
                elif nextSub.text in delNextSubtitleFixed:
                    subs.remove(sub)
#                    print('nextSub.text in delNextSubtitleFixed')
#                    currentIndexSub = currentIndexSub + 1

                else:
                    sub.text = convertToFinalSentene(sentene)
                    doneSentene = True
                    sub.start.minutes = 0
                    sub.start.seconds = timeSecondStartSub
                    timeSecondStartSub = None
                    nextSub.start.minutes = 0
                    nextSub.start.seconds = sub.end.seconds + (sub.end.minutes * 60)
                    nextSub.text = nextSub.text.replace(delNextSubtitleFixed, '', 1)
                    break
            # Truong hop text nam trong 1 sub
            elif delNextSubtitle in sub.text and  len(sub.text) != len(delNextSubtitle):
                # print('*********************')
                subtext = sub.text
                if currentIndexSub >= (len(subs) - 1):
                    timeSecondStartSub = None
                    doneSentene = True
                    break
                else:
                    nextSub = subs[currentIndexSub + 1]
                    nextSub.text = subtext.replace(sentenePre, '', 1) +' '+ nextSub.text

                    sub.text = convertToFinalSentene(sentene)
                    sub.start.minutes = 0
                    sub.start.seconds = timeSecondStartSub
                    if (nextSub.start.seconds + (sub.start.minutes * 60) ) < (sub.end.seconds + (sub.end.minutes * 60)):
                        nextSub.start.minutes = 0
                        nextSub.start.seconds = sub.end.seconds + (sub.end.minutes * 60)
                    timeSecondStartSub = None
                    doneSentene = True
            else:
                sub.text = convertToFinalSentene(sentene)
                sub.start.minutes = 0
                sub.start.seconds = timeSecondStartSub
                timeSecondStartSub = None
                doneSentene = True
        
        if currentIndexSub > 0 and currentIndexSub < len(subs) :
            prevSub = subs[currentIndexSub - 1]
            if (sub.start.seconds + (sub.start.minutes * 60)) < (prevSub.end.seconds + (prevSub.end.minutes * 60) ):
                sub.start.minutes = 0
                sub.start.seconds = prevSub.end.seconds + (prevSub.end.minutes * 60)
        currentIndexSub = currentIndexSub + 1

    path_sub_en = 'data/sub/en/'+name_file +'_en.srt'
    path_sub_vi = 'data/sub/vi/'+name_file +'_vi.srt'
    if translate_vi:
        subs.save( path_sub_en, encoding='utf-8')
        snippets.upload_blob('thuyetminh_youtube',  path_sub_en, path_sub_en)
        url_sub_en = snippets.make_blob_public('thuyetminh_youtube', path_sub_en)
        
    #    translator = Translator()
        # translate_client = translate.Client()
        translator = Translator(service_urls=[
            'translate.google.com',
            'translate.google.co.kr',
        ])
        list_text = []
        translations = []
        for sub in subs:
            # print(sub.text.encode('utf-8').strip())
            text = sub.text
            text = encode_terminology(text)
            if len(text) > 0:
                list_text.append(text)
        for i in range(int(len(list_text)/20) + 1):
            try:
                translation  = translator.translate(list_text[i*20: (i *20 +20)], dest='vi')
                translations += translation
            except :
                translations = []
                break
           
        is_free = True
        if len(translations) == 0:
            is_free = False
            translations = []
            client = translate.TranslationServiceClient()
            location = 'global'
            parent = client.location_path('fresh-aura-230118', location)
            for i in range(int(len(list_text)/20) + 1):
                response = client.translate_text(
                    parent=parent,
                    contents=list_text[i*20: (i *20 +20)],
                    mime_type='text/plain',  # mime types: text/plain, text/html
                    # source_language_code='en-US',
                    target_language_code='vi-VN')
                translations += response.translations
             
        # print(translations)
        for i in range(len(translations)):
            item = translations[i]
            strTranslator = ''
            if(is_free):
                strTranslator = item.text
            else:
                strTranslator = item.translated_text
            # print(strTranslator)
            strTranslator = decode_terminology(strTranslator)
            subs[i].text = removeSpecialCharater(strTranslator)
        
        subs.save( path_sub_vi, encoding='utf-8')
        snippets.upload_blob('thuyetminh_youtube', path_sub_vi, path_sub_vi)

        url_sub_vi = snippets.make_blob_public('thuyetminh_youtube', path_sub_vi)
        return {'url_sub_en': url_sub_en, 'url_sub_vi': url_sub_vi, 'path_sub_vi':  path_sub_vi}
    else:
        subs.save('pre_data/'+video_id+'.srt', encoding='utf-8')
    


def encode_terminology(text):
    for key, value in dict_terminology.iteritems():
        text = text.replace(value,key)
        text = text.replace(value.capitalize(),key)
        text = text.replace(value.title(),key)
    return text

def decode_terminology(text):
    for key, value in dict_terminology.iteritems():
        text = text.replace(key,value)
    return text

def subtitleAPI():
    subs = pysrt.open('stevejob.srt')
    strOutput = ''
    fistTimeSecondOfGroup = None
    global countProcess
    global totalSentenes
    totalSentenes = 0
    countProcess = 0
    sentene = []
    subIndex = 0
    lastSenteneIndex = 0
    lastestTime = None
    strSentence = ''
    for sub in subs:
        textLine = sub.text
        textLine = textLine.replace('<font color="#E5E5E5">','')
        textLine = textLine.replace('<font color="#CCCCCC">','')
        textLine = textLine.replace('</font>','')
        if lastestTime == None or (sub.start.seconds +(sub.start.minutes * 60)) == lastestTime:
            lastestTime = sub.end.seconds + (sub.end.minutes * 60);
        else:
            # print(strSentence.encode('utf-8').strip())
            strSentence = play_with_model.play(strSentence)
            strSentence = strSentence.replace('.PERIOD', '.')
            strSentence = strSentence.replace(',COMMA', ',')
            strSentence = strSentence.replace(':COLON', ':')
            strSentence = ''
            lastestTime = sub.end.seconds + (sub.end.minutes * 60);
            for i in range(lastSenteneIndex,subIndex):
                subs[i].text = strSentence

            lastSenteneIndex = subIndex;
        strSentence = strSentence + textLine
        subIndex = subIndex + 1;

def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    blob.upload_from_filename(source_file_name)
    print('File {} uploaded to {}.'.format(
                                           source_file_name,
                                           destination_blob_name))

# if __name__ == '__main__':
#     process_subtitle_to_en_vi('d6c6uIyieoo','data/sub/orginal/d6c6uIyieoo_orginal.srt', 'd6c6uIyieoo')
