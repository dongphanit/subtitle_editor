text1 = "welcome to the effortless English show , with the world's number one English teacher AJ Hoge , where AJ's more than 40 million students worldwide , finally learn English once and for all , without the boring textbooks classrooms and grammar drills"
text2 = "chào mừng bạn đến với chương trình tiếng Anh không mệt mỏi, với giáo viên tiếng Anh số một thế giới AJ Hoge nơi có hơn 40 triệu học sinh của AJ trên toàn thế giới, cuối cùng học tiếng Anh một lần và mãi mãi, mà không cần đến các lớp học và sách ngữ pháp nhàm chán"

text1 = text1.split(' ')
text2 = text2.split(' ')


set1 = set(text1)
set2 = set(text2)

common = set1.intersection(set2)
# You can even use overloaded bitwise operators to do the
# same thing
for x in range(1, 1000):
    common = set1 & set2
    print(common)


# If you want to find the items which aren't common to both
# from set1
#notInSet2 = set1.difference(set2)

## Or if you want to merge the 2 sets
#combined = set1.union(set2)

#print(combined)
