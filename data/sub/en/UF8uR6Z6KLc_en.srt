1
00:00:06,700 --> 00:00:11,410
this program is brought to you by Stanford University 

3
00:00:11,410 --> 00:00:13,888
 please visit us at stanford.edu 

6
00:00:20,339 --> 00:00:33,780
 thank you , I'm honored , to be with you today for your commencement from one of the finest universities in the world 

8
00:00:35,549 --> 00:00:46,619
 truth be told , I , never graduated from college , and this is the closest I've 

11
00:00:46,808 --> 00:00:56,709
 ever gotten to a college graduation today , I want to tell you three stories from my life : that's it no big deal just three stories 

16
00:00:56,899 --> 00:01:10,000
 the first story is about connecting the dots I dropped out of Reed College after the first six months , but then stayed around as a drop-in for another 18 months or so before , I really quit 

17
00:01:10,849 --> 00:01:14,969
 so why did I drop out it started before I was born 

20
00:01:14,349 --> 00:01:22,958
 my biological mother was a young unwed graduate student and she decided to put me up for adoption 

26
00:01:22,379 --> 00:01:37,049
 she felt very strongly that I should be adopted by college graduates , so everything was all set for me to be adopted at Birth by a lawyer and his wife , except that when I popped out , they decided at the last minute that they really wanted a girl 

30
00:01:37,409 --> 00:01:50,920
 so my parents , who were on a waiting list , got a call in the middle of the night asking we've got an unexpected baby boy 

34
00:01:50,640 --> 00:01:59,649
 do you want him ? they said , of course , my biological mother found out later that my mother had never graduated from college and that my father had never graduated from high school 

35
00:01:59,769 --> 00:02:03,399
 she refused to sign the final adoption papers 

38
00:01:59,289 --> 00:02:11,199
 she only relented a few months later , when my parents promised that I would go to college 

42
00:02:11,389 --> 00:02:24,699
 this was the start in my life and 17 years later , I did go to college , but I naively chose a college that was almost as expensive as Stanford and all of my working-class parents 

45
00:02:24,699 --> 00:02:31,030
 savings were being spent on my college tuition after six months 

46
00:02:31,568 --> 00:02:32,589
 I couldn't see the value in it 

49
00:02:32,080 --> 00:02:39,280
 I had no idea what I wanted to do with my life and no idea how college was going to help me figure it out , and here I was spending all the money 

52
00:02:39,209 --> 00:02:48,519
 my parents had saved their entire life , so I decided to drop out and trust that it would all work out 

54
00:02:48,519 --> 00:02:54,370
 okay , it was pretty scary at the time , but looking back it was one of the best decisions 

55
00:02:54,709 --> 00:02:56,920
 I ever made 

56
00:02:56,370 --> 00:02:58,810
 the minute I dropped out 

58
00:02:58,810 --> 00:03:05,080
 I could stop taking the required classes that didn't interest me and begin dropping in on the ones that looked far more interesting 

62
00:03:05,370 --> 00:03:14,860
 it wasn't all romantic : I didn't have a dorm room , so I slept on the floor in friends , rooms , I returned coke bottles for the 5 cent 

64
00:03:14,860 --> 00:03:19,450
 deposits to buy food with and I would walk the seven miles across town every Sunday night to get one good meal 

68
00:03:19,209 --> 00:03:30,189
 a week at the Hari , Krishna temple , I loved it and much of what I stumbled into by following my curiosity and intuition turned out to be priceless later on 

72
00:03:30,650 --> 00:03:40,989
 let me give you one example : Reid college at that time offered perhaps the best calligraphy instruction in the country throughout the campus 

76
00:03:40,939 --> 00:03:50,590
 every poster every label on every drawer was beautifully hand calligraphed , because I had dropped out and didn't have to take the normal classes 

77
00:03:50,739 --> 00:03:53,230
 I decided to take a calligraphy class to learn how to do this 

82
00:03:53,139 --> 00:04:05,549
 I learned about serif and sans-serif typefaces about varying the amount of space between different letter combinations about what makes great typography great 

84
00:04:05,549 --> 00:04:12,099
 it was beautiful historical artistically subtle in a way that science can't capture and I found it fascinating 

88
00:04:12,918 --> 00:04:22,359
 none of this had even a hope of any practical application in my life , but 10 years later , when we were designing the first Macintosh computer 

90
00:04:22,360 --> 00:04:27,098
 it all came back to me and we designed it all into the Mac 

95
00:04:27,478 --> 00:04:39,848
 it was the first computer with beautiful typography if I had never dropped in on that single course in college , the Mac would have never had multiple typefaces or proportionally spaced fonts and since windows just copied the Mac 

97
00:04:39,848 --> 00:04:51,159
 it's likely that no personal computer have them 

101
00:04:51,850 --> 00:04:59,110
 if I had never dropped out , I would have never dropped in on that calligraphy class and personal computers might not have the wonderful typography that they do 

104
00:04:59,240 --> 00:05:05,949
 of course , it was impossible to connect the dots looking forward when I was in college , but it was very , very clear , looking backwards 

106
00:05:05,949 --> 00:05:10,779
 ten years later again , you can't connect the dots looking forward 

109
00:05:10,569 --> 00:05:17,199
 you can only connect them looking backwards , so you have to trust that the dots will somehow connect in your future 

115
00:05:17,279 --> 00:05:38,439
 you have to trust in something your gut destiny , life karma whatever , because believing that the dots will connect down the road will give you the confidence to follow your heart , even when it leads you off the well-worn path and that will make all the difference 

116
00:05:38,829 --> 00:05:42,959
 my second story is about love and loss 

126
00:05:42,990 --> 00:06:09,040
 I was lucky : I found what I loved to do early in life , woz and I started Apple in my parent's garage when I was 20 , we worked hard and in 10 years Apple had grown from just the two of us in a garage into a two billion dollar company , with over 4,000 employees , we just released our finest creation , the Macintosh a year earlier and I just turned 30 and then I got fired 

128
00:06:09,040 --> 00:06:15,100
 how can you get fired from a company ? you started well as Apple grew 

138
00:06:15,879 --> 00:06:39,009
 we hired someone who I thought was very talented to run the company with me and for the first year or so things went well , but then our visions of the future began to diverge , and eventually we had a falling out when we did our Board of Directors sided with him , and so at 30 I was out and very publicly out what had been the focus of my entire adult life was gone and it was devastating 

139
00:06:39,279 --> 00:06:41,349
 I really didn't know what to do 

142
00:06:41,790 --> 00:06:47,040
 for a few months , I felt that I had let the previous generation of entrepreneurs down that I had dropped the baton as it was being passed to me 

145
00:06:47,990 --> 00:06:55,310
 I met with David Packard and Bob Noyce and tried to apologize for screwing up so badly 

148
00:06:55,720 --> 00:07:02,570
 I was a very public fail and I even thought about running away from the valley , but something slowly began to dawn on me , I still loved 

152
00:06:58,449 --> 00:07:16,099
 what I did the turn of events at Apple had not changed that one bit I've been rejected , but I was still in love and so I decided to start over 

155
00:07:16,418 --> 00:07:21,168
 I didn't see it then , but it turned out that getting fired from Apple was the best thing that could have ever happened to me 

158
00:07:21,360 --> 00:07:28,280
 the heaviness of being successful was replaced by the lightness of being a beginner again less sure about everything 

160
00:07:28,279 --> 00:07:32,209
 it freed me to enter one of the most creative periods of my life during the next five years 

161
00:07:32,540 --> 00:07:34,340
 I started a company named next 

164
00:07:34,379 --> 00:07:40,370
 another company named Pixar and fell in love with an amazing woman who would become my wife 

168
00:07:40,600 --> 00:07:52,160
 Pixar went on to create the world's first computer animated feature film , Toy Story and is now the most successful animation studio in the world 

177
00:07:52,160 --> 00:08:12,410
 in a remarkable turn of events , Apple bought next and I returned to Apple and the technology we developed it next is at the heart of Apple's , current Renaissance and Laureen and I have a wonderful family together , I'm , pretty sure none of this would have happened if I hadn't been fired from Apple , it was awful tasting medicine but I 

184
00:08:12,478 --> 00:08:28,399
 guess the patient needed it sometime life , sometimes life's going to hit you in the head with a brick , don't lose faith I'm convinced that the only thing that kept me going was that I loved what I did you've got to find what you love - and that is this true for work as it is for your lovers 

196
00:08:28,129 --> 00:08:55,480
 your work is going to fill a large part of your life , and the only way to be truly satisfied is to do what you believe is great work , and the only way to do great work is to love what you do if you haven't found it yet keep looking and don't settle as with all matters of the heart you'll know when you find it and like any great relationship , it just gets better and better as the years roll on so keep looking don't settle 

197
00:09:55,049 --> 00:09:11,099
 my third story is about death 

199
00:09:11,100 --> 00:09:16,470
 when I was 17 , I read a quote that went something like if you live each day as if it was your last someday 

200
00:09:16,370 --> 00:09:21,810
 you'll most certainly be right 

202
00:09:21,809 --> 00:09:26,609
 it made an impression on me and since then for the past 33 years , I've looked in the mirror 

209
00:09:26,708 --> 00:09:42,750
 every morning and asked myself if today were the last day of my life , what I want to do what I am about to do today and whenever the answer has been no for too many days in a row , I know I need to change something remembering that I'll be dead 

214
00:09:42,339 --> 00:09:54,750
 soon is the most important tool I've ever encountered to help me make the big choices in life , because almost everything all external expectations , all pride , all fear of embarrassment or failure 

218
00:09:54,429 --> 00:10:05,069
 these things just fall away in the face of death , leaving only what is truly important , remembering that you are going to die is the best way 

219
00:10:05,250 --> 00:10:07,740
 I know to avoid the trap of thinking you have something to lose 

220
00:10:07,070 --> 00:10:10,500
 you are already naked 

221
00:10:10,740 --> 00:10:14,459
 there is no reason not to follow your heart 

224
00:10:14,089 --> 00:10:23,279
 about a year ago , I was diagnosed with cancer , I had a scan at 7:30 in the morning , and it clearly showed a tumor on my pancreas 

225
00:10:23,190 --> 00:10:25,770
 I didn't even know what a pancreas was 

230
00:10:25,589 --> 00:10:37,649
 the doctors told me this was almost certainly a type of cancer that is incurable and that I should expect to live no longer than three to six months 

232
00:10:37,649 --> 00:10:44,070
 my doctor advised me to go home and get my affairs in order , which is doctors code for prepare to die 

234
00:10:44,070 --> 00:10:48,510
 it means to try and tell your kids everything you thought 

235
00:10:48,070 --> 00:10:51,600
 you'd have the next 10 years to tell them in just a few months 

238
00:10:51,190 --> 00:10:57,870
 it means to make sure everything is buttoned up , so that will be as easy as possible for your family 

243
00:10:57,149 --> 00:11:09,480
 it means to say your goodbyes I live with that diagnosis all day later that evening , I had a biopsy where they stuck an endoscope down my throat 

252
00:11:09,129 --> 00:11:40,289
 through my stomach and into my intestines put a needle into my pancreas and got a few cells from the tumor , I was sedated , but my wife , who was there , told me that when they viewed the cells under a microscope , the doctor started crying because it turned out to be a very rare form of pancreatic cancer , that is curable with surgery , I had the surgery and thankfully I'm fine 

256
00:11:40,100 --> 00:11:48,899
 now this was the closest I've been to facing death and I hope it's , the closest I get for a few more decades having lived through it 

259
00:11:48,519 --> 00:11:56,539
 I can now say this to you with a bit more certainty than when death was a useful but purely intellectual concept 

262
00:11:56,919 --> 00:12:04,948
 no one wants to die , even people who want to go to heaven don't want to die to get there and yet death is the destination 

266
00:12:04,039 --> 00:12:16,559
 we all share , no one has ever escaped it , and that is as it should be , because death is very likely the single best invention of life 

267
00:12:16,490 --> 00:12:19,318
 it's life's change agent 

269
00:12:19,318 --> 00:12:23,099
 it clears out the old to make way for the new right now 

270
00:12:23,860 --> 00:12:25,079
 the new is you , but some day , not too long from now 

271
00:12:25,100 --> 00:12:28,439
 you will gradually become the old and be cleared away 

272
00:12:28,078 --> 00:12:32,399
 sorry to be so dramatic , but it's quite true 

277
00:12:32,208 --> 00:12:43,438
 your time is limited , so don't waste it living someone else's life , don't be trapped by Dogma , which is living with the results of other people's thinking 

281
00:12:43,059 --> 00:12:52,229
 don't let the noise of others , opinions , drown out your own inner voice and most important have the courage to follow your heart and intuition 

282
00:12:52,620 --> 00:12:55,740
 they somehow already know what you truly want to become 

284
00:12:55,740 --> 00:12:59,028
 everything else is secondary 

287
00:13:59,328 --> 00:13:18,609
 when I was young , there was an amazing publication , called the Whole Earth Catalog , which was one of the Bible's of my generation 

290
00:13:18,068 --> 00:13:25,958
 it was created by a fellow named Stuart brand , not far from here in Menlo Park , and he brought it to life with his poetic touch 

294
00:13:25,578 --> 00:13:35,169
 this was in the late 60s before personal computers and desktop publishing , so it was all made with typewriters scissors and Polaroid cameras 

295
00:13:35,499 --> 00:13:37,808
 it was sort of like Google and paperback form 

296
00:13:37,169 --> 00:13:41,409
 35 years before Google came along 

297
00:13:41,808 --> 00:13:44,980
 it was idealistic overflowing with neat tools and great notions 

302
00:13:44,610 --> 00:13:55,749
 Stuart and his team put out several issues of the Whole Earth Catalog and then , when it had run its course , they put out a final issue 

303
00:13:55,879 --> 00:13:59,499
 it was the mid-1970s and I was your age on the back 

306
00:13:59,958 --> 00:14:06,458
 cover of their final issue was a photograph of an early-morning country , road , the kind you might find yourself hitchhiking on 

308
00:14:06,458 --> 00:14:13,479
 if you were so adventurous beneath it were the words stay hungry , stay foolish 

310
00:14:13,480 --> 00:14:19,928
 it was their farewell message as they signed off stay hungry 

313
00:14:19,159 --> 00:14:31,419
 stay foolish and I have always wished that for myself and now , as you graduate to begin anew , I wish that , for you stay hungry , stay foolish 

317
00:14:31,039 --> 00:14:52,099
 thank you all very much you you 

318
00:14:55,820 --> 00:15:01,160
 the preceding program is copyrighted by Stanford University 

320
00:14:58,159 --> 00:15:03,639
 please visit us at stanford.edu 

