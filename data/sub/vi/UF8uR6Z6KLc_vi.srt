1
00:00:06,700 --> 00:00:11,410
chương trình này được mang đến cho bạn bởi Đại học Stanford

3
00:00:11,410 --> 00:00:13,888
vui lòng ghé thăm chúng tôi tại stanford.edu

6
00:00:20,339 --> 00:00:33,780
cảm ơn bạn, tôi rất vinh dự được đến với bạn hôm nay vì bạn đã bắt đầu từ một trong những trường đại học tốt nhất trên thế giới

8
00:00:35,549 --> 00:00:46,619
sự thật mà nói, tôi, chưa bao giờ tốt nghiệp đại học, và đây là lần gần nhất tôi

11
00:00:46,808 --> 00:00:56,709
Hôm nay tôi tốt nghiệp đại học, tôi muốn kể cho bạn ba câu chuyện từ cuộc đời tôi: đó không phải là vấn đề lớn chỉ là ba câu chuyện

16
00:00:56,899 --> 00:01:10,000
câu chuyện đầu tiên là về việc kết nối các dấu chấm mà tôi đã bỏ học tại Đại học Sậy sau sáu tháng đầu tiên, nhưng sau đó ở lại như một người bỏ học thêm 18 tháng trước đó, tôi thực sự đã từ bỏ

17
00:01:10,849 --> 00:01:14,969
vậy tại sao tôi bỏ nó bắt đầu trước khi tôi được sinh ra

20
00:01:14,349 --> 00:01:22,958
mẹ ruột của tôi là một sinh viên mới tốt nghiệp trẻ và cô ấy quyết định cho tôi làm con nuôi

26
00:01:22,379 --> 00:01:37,049
cô ấy cảm thấy rất mạnh mẽ rằng tôi nên được nhận nuôi bởi những sinh viên tốt nghiệp đại học, vì vậy mọi thứ đã sẵn sàng để tôi được nhận nuôi bởi một luật sư và vợ anh ta, ngoại trừ khi tôi bật ra, họ quyết định vào phút cuối rằng họ thực sự muốn con gái

30
00:01:37,409 --> 00:01:50,920
Vì vậy, bố mẹ tôi, người nằm trong danh sách chờ, nhận được một cuộc gọi vào lúc nửa đêm, hỏi rằng chúng tôi có một bé trai bất ngờ

34
00:01:50,640 --> 00:01:59,649
bạn có muốn anh ta ? họ nói, tất nhiên, mẹ ruột của tôi phát hiện ra rằng mẹ tôi chưa bao giờ tốt nghiệp đại học và bố tôi chưa bao giờ tốt nghiệp trung học

35
00:01:59,769 --> 00:02:03,399
Cô đã từ chối ký vào tờ giấy làm con nuôi

38
00:01:59,289 --> 00:02:11,199
cô ấy chỉ sống lại vài tháng sau đó, khi bố mẹ tôi hứa rằng tôi sẽ đi học đại học

42
00:02:11,389 --> 00:02:24,699
đây là khởi đầu trong cuộc đời tôi và 17 năm sau, tôi đã đi học đại học, nhưng tôi ngây thơ chọn một trường đại học đắt đỏ như Stanford và tất cả các bậc cha mẹ thuộc tầng lớp lao động của tôi

45
00:02:24,699 --> 00:02:31,030
tiền tiết kiệm đã được chi cho học phí đại học của tôi sau sáu tháng

46
00:02:31,568 --> 00:02:32,589
Tôi không thể thấy giá trị của nó

49
00:02:32,080 --> 00:02:39,280
Tôi không biết mình muốn làm gì với cuộc sống của mình và không biết làm thế nào đại học sẽ giúp tôi tìm ra nó, và ở đây tôi đã tiêu hết tiền

52
00:02:39,209 --> 00:02:48,519
cha mẹ tôi đã cứu cả đời họ, vì vậy tôi quyết định bỏ học và tin tưởng rằng tất cả sẽ thành công

54
00:02:48,519 --> 00:02:54,370
được rồi, nó khá đáng sợ vào thời điểm đó, nhưng nhìn lại nó là một trong những quyết định tốt nhất

55
00:02:54,709 --> 00:02:56,920
Tôi đã từng làm

56
00:02:56,370 --> 00:02:58,810
phút tôi bỏ học

58
00:02:58,810 --> 00:03:05,080
Tôi có thể ngừng tham gia các lớp học bắt buộc không khiến tôi quan tâm và bắt đầu tham gia vào những lớp trông thú vị hơn nhiều

62
00:03:05,370 --> 00:03:14,860
Điều đó thật lãng mạn: Tôi không có phòng ký túc xá, vì vậy tôi ngủ trên sàn nhà trong bạn bè, phòng, tôi trả lại chai coca cho 5 xu

64
00:03:14,860 --> 00:03:19,450
tiền gửi để mua thức ăn và tôi sẽ đi bộ bảy dặm dọc thành phố vào đêm chủ nhật để có được một bữa ăn ngon

68
00:03:19,209 --> 00:03:30,189
một tuần ở Hari, đền thờ Krishna, tôi yêu nó và phần lớn những gì tôi vấp phải bằng cách theo dõi sự tò mò và trực giác của mình hóa ra là vô giá sau này

72
00:03:30,650 --> 00:03:40,989
hãy để tôi cho bạn một ví dụ: Reid đại học vào thời điểm đó có lẽ là hướng dẫn thư pháp tốt nhất trong cả nước trong khuôn viên trường

76
00:03:40,939 --> 00:03:50,590
mỗi poster mỗi nhãn trên mỗi ngăn kéo đều được viết bằng tay rất đẹp, vì tôi đã bỏ học và không phải học các lớp bình thường

77
00:03:50,739 --> 00:03:53,230
Tôi quyết định tham gia một lớp thư pháp để học cách làm điều này

82
00:03:53,139 --> 00:04:05,549
Tôi đã học về kiểu chữ serif và sans-serif về việc thay đổi khoảng cách giữa các kết hợp chữ cái khác nhau về những gì làm cho kiểu chữ tuyệt vời trở nên tuyệt vời

84
00:04:05,549 --> 00:04:12,099
nó đẹp một cách tinh tế về mặt nghệ thuật theo cách mà khoa học không thể nắm bắt được và tôi thấy nó thật hấp dẫn

88
00:04:12,918 --> 00:04:22,359
không ai trong số này thậm chí còn hy vọng vào bất kỳ ứng dụng thực tế nào trong cuộc sống của tôi, nhưng 10 năm sau, khi chúng tôi thiết kế máy tính Macintosh đầu tiên

90
00:04:22,360 --> 00:04:27,098
tất cả đã trở lại với tôi và chúng tôi đã thiết kế tất cả vào Mac

95
00:04:27,478 --> 00:04:39,848
Đó là máy tính đầu tiên có kiểu chữ đẹp nếu tôi chưa từng tham gia khóa học duy nhất đó ở trường đại học, Mac sẽ không bao giờ có nhiều kiểu chữ hoặc phông chữ cách đều nhau và vì các cửa sổ chỉ sao chép Mac

97
00:04:39,848 --> 00:04:51,159
có vẻ như không có máy tính cá nhân nào có chúng

101
00:04:51,850 --> 00:04:59,110
nếu tôi chưa bao giờ bỏ học, tôi sẽ không bao giờ tham gia lớp thư pháp đó và máy tính cá nhân có thể không có kiểu chữ tuyệt vời mà họ làm

104
00:04:59,240 --> 00:05:05,949
tất nhiên, không thể kết nối các dấu chấm mong đợi khi tôi học đại học, nhưng nó rất, rất rõ ràng, nhìn về phía sau

106
00:05:05,949 --> 00:05:10,779
mười năm sau một lần nữa, bạn không thể kết nối các dấu chấm mong đợi

109
00:05:10,569 --> 00:05:17,199
bạn chỉ có thể kết nối chúng nhìn về phía sau, vì vậy bạn phải tin tưởng rằng các dấu chấm sẽ kết nối bằng cách nào đó trong tương lai của bạn

115
00:05:17,279 --> 00:05:38,439
bạn phải tin vào thứ gì đó định mệnh của mình, nghiệp chướng bất cứ điều gì, bởi vì tin rằng những dấu chấm sẽ nối xuống đường sẽ giúp bạn tự tin đi theo trái tim mình, ngay cả khi nó đưa bạn ra khỏi con đường mòn và điều đó sẽ làm cho tất cả Sự khác biệt

116
00:05:38,829 --> 00:05:42,959
Câu chuyện thứ hai của tôi là về tình yêu và sự mất mát

126
00:05:42,990 --> 00:06:09,040
Tôi đã may mắn: Tôi đã tìm thấy những gì tôi thích làm từ đầu đời, woz và tôi đã bắt đầu Apple trong nhà để xe của bố mẹ tôi khi tôi 20 tuổi, chúng tôi làm việc chăm chỉ và trong 10 năm, Apple đã phát triển từ hai chúng tôi trong một gara thành một Công ty hai tỷ đô la, với hơn 4.000 nhân viên, chúng tôi vừa phát hành sáng tạo tốt nhất của mình, Macintosh một năm trước và tôi mới bước sang tuổi 30 và sau đó tôi bị sa thải

128
00:06:09,040 --> 00:06:15,100
Làm thế nào bạn có thể bị sa thải từ một công ty? bạn đã khởi đầu tốt khi Apple phát triển

138
00:06:15,879 --> 00:06:39,009
chúng tôi đã thuê một người mà tôi nghĩ là rất tài năng để điều hành công ty với tôi và trong năm đầu tiên, mọi thứ đã diễn ra tốt đẹp, nhưng rồi tầm nhìn về tương lai của chúng tôi bắt đầu chuyển hướng, và cuối cùng chúng tôi đã rơi ra khi chúng tôi làm việc Các giám đốc đứng về phía anh ta, và vì vậy ở tuổi 30 tôi đã ra ngoài và công khai những gì đã là trọng tâm của toàn bộ cuộc đời trưởng thành của tôi đã biến mất và nó đã bị tàn phá

139
00:06:39,279 --> 00:06:41,349
Tôi thực sự không biết phải làm gì

142
00:06:41,790 --> 00:06:47,040
Trong một vài tháng, tôi cảm thấy rằng tôi đã để thế hệ doanh nhân trước đó thất vọng rằng tôi đã bỏ dùi cui khi nó được truyền cho tôi

145
00:06:47,990 --> 00:06:55,310
Tôi đã gặp David Packard và Bob Noyce và cố gắng xin lỗi vì đã làm hỏng việc quá tệ

148
00:06:55,720 --> 00:07:02,570
Tôi là một người thất bại rất công khai và tôi thậm chí đã nghĩ đến việc chạy trốn khỏi thung lũng, nhưng một cái gì đó từ từ bắt đầu hé rạng với tôi, tôi vẫn yêu

152
00:06:58,449 --> 00:07:16,099
những gì tôi đã làm lần lượt các sự kiện tại Apple đã không thay đổi rằng một chút tôi đã bị từ chối, nhưng tôi vẫn còn yêu và vì vậy tôi quyết định làm lại

155
00:07:16,418 --> 00:07:21,168
Tôi đã không nhìn thấy nó sau đó, nhưng hóa ra việc bị sa thải khỏi Apple là điều tốt nhất có thể xảy ra với tôi

158
00:07:21,360 --> 00:07:28,280
sự nặng nề của thành công đã được thay thế bằng sự nhẹ nhàng của một người mới bắt đầu một lần nữa ít chắc chắn hơn về mọi thứ

160
00:07:28,279 --> 00:07:32,209
nó giải phóng tôi để bước vào một trong những giai đoạn sáng tạo nhất của cuộc đời tôi trong năm năm tới

161
00:07:32,540 --> 00:07:34,340
Tôi bắt đầu một công ty tên là tiếp theo

164
00:07:34,379 --> 00:07:40,370
một công ty khác tên Pixar và yêu một người phụ nữ tuyệt vời sẽ trở thành vợ tôi

168
00:07:40,600 --> 00:07:52,160
Pixar tiếp tục tạo ra bộ phim hoạt hình máy tính đầu tiên trên thế giới, Toy Story và hiện là hãng phim hoạt hình thành công nhất trên thế giới

177
00:07:52,160 --> 00:08:12,410
Trong một sự kiện đáng chú ý, Apple đã mua tiếp và tôi quay lại Apple và công nghệ mà chúng tôi phát triển tiếp theo là trung tâm của Apple, thời Phục hưng và Laureen và tôi có một gia đình tuyệt vời cùng nhau, tôi chắc chắn không ai trong số này sẽ xảy ra nếu tôi không bị sa thải khỏi Apple, đó là thuốc nếm kinh khủng nhưng tôi

184
00:08:12,478 --> 00:08:28,399
đoán bệnh nhân cần nó đôi khi cuộc sống, đôi khi cuộc sống sẽ đập vào đầu bạn bằng cục gạch, đừng mất niềm tin Tôi tin rằng điều duy nhất khiến tôi tiếp tục là tôi yêu những gì tôi đã làm tìm thấy những gì bạn yêu thích - và điều đó đúng với công việc vì nó dành cho những người yêu thích của bạn

196
00:08:28,129 --> 00:08:55,480
công việc của bạn sẽ chiếm phần lớn cuộc sống của bạn, và cách duy nhất để thực sự hài lòng là làm những gì bạn tin là công việc tuyệt vời, và cách duy nhất để làm công việc tuyệt vời là yêu những gì bạn làm nếu bạn không tìm thấy nó nhưng vẫn tiếp tục tìm kiếm và không giải quyết như với tất cả các vấn đề của trái tim bạn sẽ biết khi bạn tìm thấy nó và giống như bất kỳ mối quan hệ tuyệt vời nào, nó sẽ trở nên tốt hơn và tốt hơn khi năm tháng trôi qua vì vậy hãy tiếp tục tìm kiếm

197
00:09:55,049 --> 00:09:11,099
Câu chuyện thứ ba của tôi là về cái chết

199
00:09:11,100 --> 00:09:16,470
Khi tôi 17 tuổi, tôi đã đọc một câu trích dẫn giống như khi bạn sống mỗi ngày như thể đó là ngày cuối cùng của bạn

200
00:09:16,370 --> 00:09:21,810
bạn chắc chắn sẽ đúng

202
00:09:21,809 --> 00:09:26,609
nó đã gây ấn tượng với tôi và kể từ đó trong 33 năm qua, tôi đã nhìn vào gương

209
00:09:26,708 --> 00:09:42,750
Mỗi buổi sáng và tự hỏi mình rằng hôm nay có phải là ngày cuối cùng của cuộc đời tôi không, tôi muốn làm gì trong ngày hôm nay và bất cứ khi nào câu trả lời không có quá nhiều ngày liên tiếp, tôi biết tôi cần thay đổi một điều gì đó rằng tôi sẽ chết

214
00:09:42,339 --> 00:09:54,750
sớm là công cụ quan trọng nhất tôi từng gặp để giúp tôi đưa ra những lựa chọn lớn trong cuộc sống, bởi vì hầu hết mọi thứ đều là kỳ vọng bên ngoài, tất cả là niềm tự hào, tất cả là sợ bối rối hay thất bại

218
00:09:54,429 --> 00:10:05,069
những điều này sẽ biến mất khi đối mặt với cái chết, chỉ để lại những gì thực sự quan trọng, hãy nhớ rằng bạn sẽ chết là cách tốt nhất

219
00:10:05,250 --> 00:10:07,740
Tôi biết để tránh cái bẫy nghĩ rằng bạn có gì đó để mất

220
00:10:07,070 --> 00:10:10,500
bạn đã trần truồng

221
00:10:10,740 --> 00:10:14,459
Không có lý do để không đi theo trái tim của bạn

224
00:10:14,089 --> 00:10:23,279
Khoảng một năm trước, tôi được chẩn đoán mắc bệnh ung thư, tôi đã chụp vào lúc 7:30 sáng và rõ ràng cho thấy một khối u trên tuyến tụy của tôi

225
00:10:23,190 --> 00:10:25,770
Tôi thậm chí không biết tuyến tụy là gì

230
00:10:25,589 --> 00:10:37,649
Các bác sĩ nói với tôi rằng đây gần như chắc chắn là một loại ung thư không thể chữa được và tôi sẽ sống không quá ba đến sáu tháng

232
00:10:37,649 --> 00:10:44,070
Bác sĩ của tôi khuyên tôi nên về nhà và sắp xếp công việc của mình theo thứ tự, đó là mã bác sĩ chuẩn bị chết

234
00:10:44,070 --> 00:10:48,510
nó có nghĩa là cố gắng và nói với con bạn mọi thứ bạn nghĩ

235
00:10:48,070 --> 00:10:51,600
bạn sẽ có 10 năm tới để nói với họ chỉ sau vài tháng

238
00:10:51,190 --> 00:10:57,870
điều đó có nghĩa là đảm bảo mọi thứ đều được cài đặt, vì vậy điều đó sẽ dễ dàng nhất có thể cho gia đình bạn

243
00:10:57,149 --> 00:11:09,480
có nghĩa là nói lời tạm biệt của bạn Tôi sống với chẩn đoán đó cả ngày sau buổi tối hôm đó, tôi đã sinh thiết nơi họ đặt ống nội soi xuống cổ họng của tôi

252
00:11:09,129 --> 00:11:40,289
qua dạ dày và vào ruột tôi đặt một cây kim vào tuyến tụy và lấy một vài tế bào từ khối u, tôi đã được an thần, nhưng vợ tôi, người ở đó, nói với tôi rằng khi họ nhìn các tế bào dưới kính hiển vi, bác sĩ bắt đầu khóc bởi vì nó hóa ra là một dạng ung thư tuyến tụy rất hiếm gặp, có thể chữa được bằng phẫu thuật, tôi đã phẫu thuật và rất may tôi vẫn ổn

256
00:11:40,100 --> 00:11:48,899
bây giờ đây là lần gần nhất tôi phải đối mặt với cái chết và tôi hy vọng nó là lần gần nhất tôi có được trong vài thập kỷ sống qua nó

259
00:11:48,519 --> 00:11:56,539
Bây giờ tôi có thể nói điều này với bạn một cách chắc chắn hơn một chút so với khi cái chết là một khái niệm hữu ích nhưng hoàn toàn trí tuệ

262
00:11:56,919 --> 00:12:04,948
không ai muốn chết, ngay cả những người muốn lên thiên đàng cũng không muốn chết để đến đó và cái chết là đích đến

266
00:12:04,039 --> 00:12:16,559
tất cả chúng ta đều chia sẻ, không ai từng thoát khỏi nó, và đó là điều nên làm, bởi vì cái chết rất có thể là phát minh tốt nhất của sự sống

267
00:12:16,490 --> 00:12:19,318
đó là tác nhân thay đổi cuộc sống

269
00:12:19,318 --> 00:12:23,099
nó dọn sạch cái cũ để dọn đường cho cái mới ngay bây giờ

270
00:12:23,860 --> 00:12:25,079
người mới là bạn, nhưng một ngày nào đó, không quá lâu kể từ bây giờ

271
00:12:25,100 --> 00:12:28,439
bạn sẽ dần trở nên cũ và bị xóa đi

272
00:12:28,078 --> 00:12:32,399
xin lỗi vì quá kịch tính, nhưng nó hoàn toàn đúng

277
00:12:32,208 --> 00:12:43,438
thời gian của bạn bị hạn chế, vì vậy đừng lãng phí cuộc sống của người khác, đừng bị mắc kẹt bởi Dogma, người đang sống với kết quả của suy nghĩ của người khác

281
00:12:43,059 --> 00:12:52,229
đừng để tiếng ồn của người khác, ý kiến, nhấn chìm tiếng nói bên trong của bạn và quan trọng nhất là có can đảm để làm theo trái tim và trực giác của bạn

282
00:12:52,620 --> 00:12:55,740
họ bằng cách nào đó đã biết những gì bạn thực sự muốn trở thành

284
00:12:55,740 --> 00:12:59,028
mọi thứ khác chỉ là thứ yếu

287
00:13:59,328 --> 00:13:18,609
khi tôi còn trẻ, có một ấn phẩm tuyệt vời, được gọi là Toàn bộ danh mục Trái đất, là một trong những cuốn Kinh thánh thuộc thế hệ của tôi

290
00:13:18,068 --> 00:13:25,958
nó được tạo ra bởi một người có thương hiệu Stuart, cách Menlo Park không xa, và anh ấy đã mang nó vào cuộc sống với nét thơ mộng của mình

294
00:13:25,578 --> 00:13:35,169
đó là vào cuối những năm 60 trước khi máy tính cá nhân và máy tính để bàn xuất bản, vì vậy tất cả được làm bằng kéo máy chữ và máy ảnh Polaroid

295
00:13:35,499 --> 00:13:37,808
nó giống như Google và dạng bìa mềm

296
00:13:37,169 --> 00:13:41,409
35 năm trước khi Google ra đời

297
00:13:41,808 --> 00:13:44,980
đó là lý tưởng tràn đầy các công cụ gọn gàng và các khái niệm tuyệt vời

302
00:13:44,610 --> 00:13:55,749
Stuart và nhóm của ông đã đưa ra một số vấn đề của Toàn bộ danh mục Trái đất và sau đó, khi nó chạy hết khóa học, họ đã đưa ra một vấn đề cuối cùng

303
00:13:55,879 --> 00:13:59,499
đó là giữa những năm 1970 và tôi bằng tuổi bạn

306
00:13:59,958 --> 00:14:06,458
Ảnh bìa của vấn đề cuối cùng của họ là một bức ảnh về một đất nước sáng sớm, con đường, loại bạn có thể thấy mình quá giang trên

308
00:14:06,458 --> 00:14:13,479
nếu bạn quá phiêu lưu bên dưới thì đó là những từ đói, hãy cứ dại khờ

310
00:14:13,480 --> 00:14:19,928
đó là tin nhắn chia tay của họ khi họ ký tắt đói

313
00:14:19,159 --> 00:14:31,419
hãy cứ dại khờ và tôi đã luôn ước điều đó cho bản thân mình và bây giờ, khi bạn tốt nghiệp để bắt đầu lại, tôi ước rằng, vì bạn luôn đói, hãy cứ dại khờ

317
00:14:31,039 --> 00:14:52,099
cảm ơn tất cả các bạn rất nhiều bạn

318
00:14:55,820 --> 00:15:01,160
chương trình trước có bản quyền của Đại học Stanford

320
00:14:58,159 --> 00:15:03,639
vui lòng ghé thăm chúng tôi tại stanford.edu

