﻿1
00:00:06,700 --> 00:00:11,410
this program is<font color="#E5E5E5"> brought</font><font color="#CCCCCC"> to you by</font>

2
00:00:08,769 --> 00:00:13,888
Stanford<font color="#E5E5E5"> University</font><font color="#CCCCCC"> please visit</font><font color="#E5E5E5"> us at</font>

3
00:00:11,410 --> 00:00:13,888
stanford.edu

4
00:00:20,429 --> 00:00:30,339
thank you I'm honored<font color="#CCCCCC"> to</font><font color="#E5E5E5"> be</font><font color="#CCCCCC"> with</font><font color="#E5E5E5"> you</font>

5
00:00:28,809 --> 00:00:33,780
today for<font color="#E5E5E5"> your commencement</font><font color="#CCCCCC"> from one of</font>

6
00:00:30,339 --> 00:00:33,780
the finest universities<font color="#CCCCCC"> in the world</font>

7
00:00:35,128 --> 00:00:43,420
truth be told I never<font color="#E5E5E5"> graduated</font><font color="#CCCCCC"> from</font>

8
00:00:39,549 --> 00:00:46,619
<font color="#CCCCCC">college</font><font color="#E5E5E5"> and this is the closest I've</font>

9
00:00:43,420 --> 00:00:46,620
ever gotten to a college graduation

10
00:00:47,219 --> 00:00:52,659
<font color="#E5E5E5">today I want to tell you three</font><font color="#CCCCCC"> stories</font>

11
00:00:49,808 --> 00:00:56,709
from my<font color="#E5E5E5"> life that's it</font><font color="#CCCCCC"> no big deal</font>

12
00:00:52,659 --> 00:01:01,359
<font color="#CCCCCC">just three stories</font><font color="#E5E5E5"> the first story is</font>

13
00:00:56,710 --> 00:01:03,250
about<font color="#E5E5E5"> connecting the</font><font color="#CCCCCC"> dots I dropped out</font>

14
00:01:01,359 --> 00:01:04,899
of Reed<font color="#E5E5E5"> College</font><font color="#CCCCCC"> after the first six</font>

15
00:01:03,250 --> 00:01:06,849
<font color="#CCCCCC">months but then stayed around as a</font>

16
00:01:04,899 --> 00:01:10,000
<font color="#CCCCCC">drop-in for</font><font color="#E5E5E5"> another 18 months</font><font color="#CCCCCC"> or so</font>

17
00:01:06,849 --> 00:01:14,969
before I really<font color="#CCCCCC"> quit</font><font color="#E5E5E5"> so why did I drop</font>

18
00:01:10,000 --> 00:01:17,349
out it started<font color="#E5E5E5"> before I was born</font><font color="#CCCCCC"> my</font>

19
00:01:14,969 --> 00:01:20,079
<font color="#CCCCCC">biological mother was a young unwed</font>

20
00:01:17,349 --> 00:01:22,958
graduate student and<font color="#E5E5E5"> she</font><font color="#CCCCCC"> decided to</font><font color="#E5E5E5"> put</font>

21
00:01:20,079 --> 00:01:24,640
<font color="#E5E5E5">me up</font><font color="#CCCCCC"> for adoption she felt very</font>

22
00:01:22,959 --> 00:01:26,829
strongly that<font color="#E5E5E5"> I</font><font color="#CCCCCC"> should be adopted by</font>

23
00:01:24,640 --> 00:01:29,170
college graduates<font color="#E5E5E5"> so</font><font color="#CCCCCC"> everything</font><font color="#E5E5E5"> was all</font>

24
00:01:26,829 --> 00:01:32,379
set for me<font color="#CCCCCC"> to be adopted at Birth by a</font>

25
00:01:29,170 --> 00:01:34,420
lawyer and<font color="#CCCCCC"> his</font><font color="#E5E5E5"> wife except that when I</font>

26
00:01:32,379 --> 00:01:37,049
popped out<font color="#E5E5E5"> they decided at the last</font>

27
00:01:34,420 --> 00:01:39,459
<font color="#E5E5E5">minute that they really wanted a girl</font><font color="#CCCCCC"> so</font>

28
00:01:37,049 --> 00:01:41,409
<font color="#CCCCCC">my parents</font><font color="#E5E5E5"> who were on a waiting list</font>

29
00:01:39,459 --> 00:01:44,890
<font color="#CCCCCC">got a call in the</font><font color="#E5E5E5"> middle of the night</font>

30
00:01:41,409 --> 00:01:50,920
<font color="#CCCCCC">asking</font><font color="#E5E5E5"> we've got an unexpected baby boy</font>

31
00:01:44,890 --> 00:01:52,868
<font color="#E5E5E5">do you want him they said of course my</font>

32
00:01:50,920 --> 00:01:54,640
<font color="#CCCCCC">biological mother found</font><font color="#E5E5E5"> out later that</font>

33
00:01:52,868 --> 00:01:56,769
my<font color="#CCCCCC"> mother had never graduated</font><font color="#E5E5E5"> from</font>

34
00:01:54,640 --> 00:01:59,649
<font color="#CCCCCC">college</font><font color="#E5E5E5"> and that my father had never</font>

35
00:01:56,769 --> 00:02:03,399
<font color="#CCCCCC">graduated from high school she refused</font>

36
00:01:59,649 --> 00:02:05,289
<font color="#E5E5E5">to sign the</font><font color="#CCCCCC"> final adoption papers she</font>

37
00:02:03,399 --> 00:02:07,629
only relented a few months later<font color="#E5E5E5"> when</font><font color="#CCCCCC"> my</font>

38
00:02:05,289 --> 00:02:11,199
parents promised that<font color="#CCCCCC"> I would go</font><font color="#E5E5E5"> to</font>

39
00:02:07,629 --> 00:02:16,530
college<font color="#CCCCCC"> this was the start in my life</font>

40
00:02:11,199 --> 00:02:19,389
<font color="#CCCCCC">and 17 years later I did go</font><font color="#E5E5E5"> to</font><font color="#CCCCCC"> college</font>

41
00:02:16,530 --> 00:02:22,688
but I naively chose a college<font color="#CCCCCC"> that was</font>

42
00:02:19,389 --> 00:02:24,699
almost<font color="#E5E5E5"> as</font><font color="#CCCCCC"> expensive as Stanford and all</font>

43
00:02:22,688 --> 00:02:27,699
of my<font color="#E5E5E5"> working-class parents savings were</font>

44
00:02:24,699 --> 00:02:28,568
being spent<font color="#E5E5E5"> on my college</font><font color="#CCCCCC"> tuition</font><font color="#E5E5E5"> after</font>

45
00:02:27,699 --> 00:02:31,030
six<font color="#E5E5E5"> months</font>

46
00:02:28,568 --> 00:02:32,589
I couldn't see the value<font color="#E5E5E5"> in it</font><font color="#CCCCCC"> I had no</font>

47
00:02:31,030 --> 00:02:35,080
<font color="#E5E5E5">idea what</font><font color="#CCCCCC"> I wanted to do with my life</font>

48
00:02:32,590 --> 00:02:37,390
<font color="#E5E5E5">and no idea how college was</font><font color="#CCCCCC"> going to</font>

49
00:02:35,080 --> 00:02:39,280
help me<font color="#CCCCCC"> figure it</font><font color="#E5E5E5"> out and here I was</font>

50
00:02:37,389 --> 00:02:43,208
<font color="#E5E5E5">spending all the</font><font color="#CCCCCC"> money my parents had</font>

51
00:02:39,280 --> 00:02:45,340
saved their entire life so I decided<font color="#E5E5E5"> to</font>

52
00:02:43,209 --> 00:02:48,519
drop out and trust<font color="#CCCCCC"> that</font><font color="#E5E5E5"> it would all</font>

53
00:02:45,340 --> 00:02:50,709
work<font color="#E5E5E5"> out okay it was pretty scary</font><font color="#CCCCCC"> at</font><font color="#E5E5E5"> the</font>

54
00:02:48,519 --> 00:02:54,370
<font color="#CCCCCC">time but looking back it was one</font><font color="#E5E5E5"> of the</font>

55
00:02:50,709 --> 00:02:56,920
best decisions<font color="#CCCCCC"> I ever made</font><font color="#E5E5E5"> the minute I</font>

56
00:02:54,370 --> 00:02:58,810
dropped out I could stop<font color="#E5E5E5"> taking the</font>

57
00:02:56,919 --> 00:03:01,629
required<font color="#E5E5E5"> classes that didn't interest me</font>

58
00:02:58,810 --> 00:03:05,080
and begin<font color="#E5E5E5"> dropping in on the ones that</font>

59
00:03:01,629 --> 00:03:07,209
<font color="#CCCCCC">looked far more interesting it</font><font color="#E5E5E5"> wasn't</font>

60
00:03:05,080 --> 00:03:09,370
all romantic<font color="#CCCCCC"> I didn't have a dorm room</font>

61
00:03:07,209 --> 00:03:12,340
<font color="#CCCCCC">so I slept</font><font color="#E5E5E5"> on the</font><font color="#CCCCCC"> floor in friends rooms</font>

62
00:03:09,370 --> 00:03:14,860
<font color="#CCCCCC">I returned coke bottles for</font><font color="#E5E5E5"> the 5 cent</font>

63
00:03:12,340 --> 00:03:16,509
deposits to buy food with and I would

64
00:03:14,860 --> 00:03:19,450
walk the seven<font color="#E5E5E5"> miles across town</font><font color="#CCCCCC"> every</font>

65
00:03:16,509 --> 00:03:22,659
<font color="#E5E5E5">Sunday night to get one good meal a week</font>

66
00:03:19,449 --> 00:03:25,209
at the Hari Krishna temple<font color="#CCCCCC"> I loved it</font>

67
00:03:22,659 --> 00:03:27,189
and much of<font color="#E5E5E5"> what I stumbled into by</font>

68
00:03:25,209 --> 00:03:30,189
<font color="#CCCCCC">following my curiosity and intuition</font>

69
00:03:27,189 --> 00:03:33,879
<font color="#CCCCCC">turned</font><font color="#E5E5E5"> out to be priceless later on let</font>

70
00:03:30,189 --> 00:03:35,650
<font color="#E5E5E5">me give you one example</font><font color="#CCCCCC"> Reid college at</font>

71
00:03:33,879 --> 00:03:37,479
that<font color="#E5E5E5"> time</font><font color="#CCCCCC"> offered perhaps the best</font>

72
00:03:35,650 --> 00:03:40,989
calligraphy<font color="#E5E5E5"> instruction in the country</font>

73
00:03:37,479 --> 00:03:42,789
throughout the campus every poster<font color="#E5E5E5"> every</font>

74
00:03:40,989 --> 00:03:45,939
label on every drawer<font color="#E5E5E5"> was beautifully</font>

75
00:03:42,789 --> 00:03:47,739
hand calligraphed<font color="#E5E5E5"> because I had dropped</font>

76
00:03:45,939 --> 00:03:50,590
<font color="#E5E5E5">out and didn't have to take the normal</font>

77
00:03:47,739 --> 00:03:53,230
<font color="#CCCCCC">classes I decided to take a calligraphy</font>

78
00:03:50,590 --> 00:03:55,359
class to learn<font color="#E5E5E5"> how to do this</font><font color="#CCCCCC"> I learned</font>

79
00:03:53,229 --> 00:03:57,369
about serif and sans-serif typefaces

80
00:03:55,359 --> 00:03:59,139
about varying the amount of space

81
00:03:57,370 --> 00:04:02,250
<font color="#CCCCCC">between different letter combinations</font>

82
00:03:59,139 --> 00:04:05,549
about<font color="#E5E5E5"> what makes great typography great</font>

83
00:04:02,250 --> 00:04:08,049
it was beautiful historical<font color="#CCCCCC"> artistically</font>

84
00:04:05,549 --> 00:04:12,099
subtle<font color="#E5E5E5"> in a way that science can't</font>

85
00:04:08,049 --> 00:04:14,099
capture<font color="#E5E5E5"> and I found it fascinating</font><font color="#CCCCCC"> none</font>

86
00:04:12,098 --> 00:04:17,918
<font color="#CCCCCC">of this had even</font><font color="#E5E5E5"> a hope of any practical</font>

87
00:04:14,098 --> 00:04:19,930
application<font color="#E5E5E5"> in my life but 10</font><font color="#CCCCCC"> years</font>

88
00:04:17,918 --> 00:04:22,359
<font color="#E5E5E5">later when</font><font color="#CCCCCC"> we</font><font color="#E5E5E5"> were</font><font color="#CCCCCC"> designing the first</font>

89
00:04:19,930 --> 00:04:25,150
Macintosh computer it all<font color="#E5E5E5"> came back to</font>

90
00:04:22,360 --> 00:04:27,098
<font color="#CCCCCC">me and we designed it all into the Mac</font>

91
00:04:25,149 --> 00:04:30,549
it was<font color="#E5E5E5"> the first computer with beautiful</font>

92
00:04:27,098 --> 00:04:32,978
typography<font color="#CCCCCC"> if I had never</font><font color="#E5E5E5"> dropped in on</font>

93
00:04:30,550 --> 00:04:34,478
<font color="#CCCCCC">that single</font><font color="#E5E5E5"> course in college</font><font color="#CCCCCC"> the Mac</font>

94
00:04:32,978 --> 00:04:37,509
<font color="#E5E5E5">would have never had multiple typefaces</font>

95
00:04:34,478 --> 00:04:39,848
or proportionally spaced fonts<font color="#E5E5E5"> and since</font>

96
00:04:37,509 --> 00:04:41,230
windows just copied the Mac it's likely

97
00:04:39,848 --> 00:04:51,159
<font color="#CCCCCC">that no personal computer</font>

98
00:04:41,230 --> 00:04:52,600
have them if I had<font color="#E5E5E5"> never dropped out I</font>

99
00:04:51,160 --> 00:04:54,850
would<font color="#E5E5E5"> have never dropped in on</font><font color="#CCCCCC"> that</font>

100
00:04:52,600 --> 00:04:56,260
calligraphy class and personal computers

101
00:04:54,850 --> 00:04:59,110
might not have the<font color="#CCCCCC"> wonderful typography</font>

102
00:04:56,259 --> 00:05:01,240
<font color="#E5E5E5">that they do of course it was impossible</font>

103
00:04:59,110 --> 00:05:03,730
<font color="#CCCCCC">to connect the dots looking forward when</font>

104
00:05:01,240 --> 00:05:05,949
I was in college<font color="#CCCCCC"> but</font><font color="#E5E5E5"> it was very very</font>

105
00:05:03,730 --> 00:05:09,009
clear<font color="#E5E5E5"> looking backwards</font><font color="#CCCCCC"> ten years</font><font color="#E5E5E5"> later</font>

106
00:05:05,949 --> 00:05:10,779
<font color="#E5E5E5">again</font><font color="#CCCCCC"> you can't connect the dots looking</font>

107
00:05:09,009 --> 00:05:13,569
forward you<font color="#E5E5E5"> can only connect them</font>

108
00:05:10,779 --> 00:05:15,309
looking backwards<font color="#E5E5E5"> so you have to trust</font>

109
00:05:13,569 --> 00:05:17,199
that the<font color="#CCCCCC"> dots</font><font color="#E5E5E5"> will somehow connect in</font>

110
00:05:15,310 --> 00:05:19,689
your future<font color="#CCCCCC"> you have to trust in</font>

111
00:05:17,199 --> 00:05:22,569
<font color="#CCCCCC">something your</font><font color="#E5E5E5"> gut destiny life karma</font>

112
00:05:19,689 --> 00:05:25,360
whatever because believing that the dots

113
00:05:22,569 --> 00:05:27,279
<font color="#E5E5E5">will connect down the road will give you</font>

114
00:05:25,360 --> 00:05:29,830
the confidence<font color="#E5E5E5"> to follow your heart even</font>

115
00:05:27,279 --> 00:05:38,439
<font color="#CCCCCC">when it leads you off the well-worn path</font>

116
00:05:29,829 --> 00:05:42,959
and that<font color="#E5E5E5"> will</font><font color="#CCCCCC"> make</font><font color="#E5E5E5"> all the</font><font color="#CCCCCC"> difference my</font>

117
00:05:38,439 --> 00:05:46,240
second<font color="#E5E5E5"> story is about</font><font color="#CCCCCC"> love and loss I</font>

118
00:05:42,959 --> 00:05:49,269
was<font color="#E5E5E5"> lucky</font><font color="#CCCCCC"> I found what I loved to do</font>

119
00:05:46,240 --> 00:05:51,670
early in<font color="#CCCCCC"> life</font><font color="#E5E5E5"> woz</font><font color="#CCCCCC"> and I</font><font color="#E5E5E5"> started Apple in</font>

120
00:05:49,269 --> 00:05:53,889
my<font color="#E5E5E5"> parent's garage when I was 20 we</font>

121
00:05:51,670 --> 00:05:55,240
worked<font color="#CCCCCC"> hard</font><font color="#E5E5E5"> and in 10 years Apple had</font>

122
00:05:53,889 --> 00:05:57,639
grown<font color="#CCCCCC"> from just the two of us in a</font>

123
00:05:55,240 --> 00:06:00,280
<font color="#CCCCCC">garage</font><font color="#E5E5E5"> into a two billion dollar company</font>

124
00:05:57,639 --> 00:06:01,990
with<font color="#CCCCCC"> over</font><font color="#E5E5E5"> 4,000 employees we just</font>

125
00:06:00,279 --> 00:06:04,029
released<font color="#E5E5E5"> our finest creation the</font>

126
00:06:01,990 --> 00:06:09,040
Macintosh<font color="#CCCCCC"> a year earlier and I just</font>

127
00:06:04,029 --> 00:06:11,250
turned 30<font color="#CCCCCC"> and then I got fired</font><font color="#E5E5E5"> how can</font>

128
00:06:09,040 --> 00:06:15,100
you get fired<font color="#E5E5E5"> from a company you started</font>

129
00:06:11,250 --> 00:06:17,170
<font color="#E5E5E5">well as Apple grew we hired someone</font><font color="#CCCCCC"> who</font>

130
00:06:15,100 --> 00:06:18,939
I thought<font color="#E5E5E5"> was very talented</font><font color="#CCCCCC"> to</font><font color="#E5E5E5"> run the</font>

131
00:06:17,170 --> 00:06:21,100
<font color="#CCCCCC">company with me and for the first year</font>

132
00:06:18,939 --> 00:06:22,750
<font color="#CCCCCC">or</font><font color="#E5E5E5"> so things went</font><font color="#CCCCCC"> well</font><font color="#E5E5E5"> but then our</font>

133
00:06:21,100 --> 00:06:25,510
visions of the future began to diverge

134
00:06:22,750 --> 00:06:27,670
and eventually we had a falling out when

135
00:06:25,509 --> 00:06:31,300
<font color="#E5E5E5">we did our Board of Directors sided with</font>

136
00:06:27,670 --> 00:06:33,879
him<font color="#E5E5E5"> and so at 30 I was out and very</font>

137
00:06:31,300 --> 00:06:36,280
publicly out what had been the focus<font color="#CCCCCC"> of</font>

138
00:06:33,879 --> 00:06:39,009
my entire<font color="#E5E5E5"> adult life was gone and it was</font>

139
00:06:36,279 --> 00:06:41,349
devastating I really didn't know what<font color="#CCCCCC"> to</font>

140
00:06:39,009 --> 00:06:42,789
do for<font color="#E5E5E5"> a few months</font><font color="#CCCCCC"> I felt that</font><font color="#E5E5E5"> I had</font>

141
00:06:41,350 --> 00:06:44,950
<font color="#E5E5E5">let the previous generation of</font>

142
00:06:42,790 --> 00:06:47,040
entrepreneurs<font color="#E5E5E5"> down that I had dropped</font>

143
00:06:44,949 --> 00:06:49,990
the<font color="#E5E5E5"> baton as it was being passed to me I</font>

144
00:06:47,040 --> 00:06:52,480
met with David Packard and<font color="#E5E5E5"> Bob Noyce and</font>

145
00:06:49,990 --> 00:06:55,310
tried to<font color="#E5E5E5"> apologize for screwing up so</font>

146
00:06:52,480 --> 00:06:56,720
<font color="#CCCCCC">badly I was a very public fail</font>

147
00:06:55,310 --> 00:06:58,970
and I<font color="#E5E5E5"> even thought about running away</font>

148
00:06:56,720 --> 00:07:02,570
<font color="#E5E5E5">from the valley but something slowly</font>

149
00:06:58,970 --> 00:07:06,020
<font color="#E5E5E5">began to dawn on me</font><font color="#CCCCCC"> I still loved what I</font>

150
00:07:02,569 --> 00:07:08,449
did<font color="#E5E5E5"> the turn of events at Apple had not</font>

151
00:07:06,019 --> 00:07:12,379
<font color="#E5E5E5">changed that one bit I've been</font><font color="#CCCCCC"> rejected</font>

152
00:07:08,449 --> 00:07:16,099
<font color="#CCCCCC">but I was still</font><font color="#E5E5E5"> in</font><font color="#CCCCCC"> love and so I decided</font>

153
00:07:12,379 --> 00:07:17,418
to<font color="#E5E5E5"> start over</font><font color="#CCCCCC"> I didn't see</font><font color="#E5E5E5"> it then but</font>

154
00:07:16,100 --> 00:07:18,710
<font color="#CCCCCC">it</font><font color="#E5E5E5"> turned out that</font><font color="#CCCCCC"> getting fired from</font>

155
00:07:17,418 --> 00:07:21,168
Apple was the<font color="#E5E5E5"> best thing</font><font color="#CCCCCC"> that could have</font>

156
00:07:18,709 --> 00:07:23,359
ever<font color="#CCCCCC"> happened to me</font><font color="#E5E5E5"> the heaviness of</font>

157
00:07:21,168 --> 00:07:25,969
<font color="#CCCCCC">being successful was</font><font color="#E5E5E5"> replaced</font><font color="#CCCCCC"> by the</font>

158
00:07:23,360 --> 00:07:28,280
<font color="#E5E5E5">lightness of being a beginner again less</font>

159
00:07:25,970 --> 00:07:29,540
sure about<font color="#CCCCCC"> everything it freed me</font><font color="#E5E5E5"> to</font>

160
00:07:28,279 --> 00:07:32,209
<font color="#E5E5E5">enter one of the</font><font color="#CCCCCC"> most creative periods</font>

161
00:07:29,540 --> 00:07:34,340
of my<font color="#E5E5E5"> life during</font><font color="#CCCCCC"> the</font><font color="#E5E5E5"> next five years I</font>

162
00:07:32,209 --> 00:07:36,379
started<font color="#E5E5E5"> a company named next another</font>

163
00:07:34,339 --> 00:07:37,699
company named Pixar and fell in love

164
00:07:36,379 --> 00:07:40,370
<font color="#CCCCCC">with an amazing woman who would become</font>

165
00:07:37,699 --> 00:07:42,050
<font color="#CCCCCC">my wife Pixar went on to</font><font color="#E5E5E5"> create the</font>

166
00:07:40,370 --> 00:07:44,600
world's first<font color="#E5E5E5"> computer animated feature</font>

167
00:07:42,050 --> 00:07:48,520
film<font color="#E5E5E5"> Toy Story</font><font color="#CCCCCC"> and is now</font><font color="#E5E5E5"> the most</font>

168
00:07:44,600 --> 00:07:52,160
successful animation studio in the world

169
00:07:48,519 --> 00:07:54,469
<font color="#E5E5E5">in a remarkable turn of events Apple</font>

170
00:07:52,160 --> 00:07:56,660
bought next and I returned to<font color="#E5E5E5"> Apple and</font>

171
00:07:54,470 --> 00:07:58,040
the technology we developed it next is

172
00:07:56,660 --> 00:08:00,740
at the heart of<font color="#E5E5E5"> Apple's current</font>

173
00:07:58,040 --> 00:08:03,890
Renaissance and<font color="#CCCCCC"> Laureen and I have a</font>

174
00:08:00,740 --> 00:08:05,750
<font color="#E5E5E5">wonderful family together</font><font color="#CCCCCC"> I'm pretty</font>

175
00:08:03,889 --> 00:08:07,159
sure none<font color="#CCCCCC"> of this would have happened</font><font color="#E5E5E5"> if</font>

176
00:08:05,750 --> 00:08:09,560
I hadn't been<font color="#E5E5E5"> fired from Apple</font>

177
00:08:07,160 --> 00:08:12,410
it was awful tasting medicine<font color="#E5E5E5"> but I</font>

178
00:08:09,560 --> 00:08:14,300
guess the patient needed it<font color="#CCCCCC"> sometime</font>

179
00:08:12,410 --> 00:08:16,340
life<font color="#E5E5E5"> sometimes life's going to hit you</font>

180
00:08:14,300 --> 00:08:18,949
in<font color="#CCCCCC"> the head with a brick don't</font><font color="#E5E5E5"> lose</font>

181
00:08:16,339 --> 00:08:20,750
faith<font color="#CCCCCC"> I'm convinced that the only thing</font>

182
00:08:18,949 --> 00:08:23,478
<font color="#E5E5E5">that kept me</font><font color="#CCCCCC"> going</font><font color="#E5E5E5"> was that I loved what</font>

183
00:08:20,750 --> 00:08:26,000
<font color="#CCCCCC">I did you've got to find what you love</font>

184
00:08:23,478 --> 00:08:28,399
and<font color="#CCCCCC"> that is</font><font color="#E5E5E5"> this true</font><font color="#CCCCCC"> for work as it is</font>

185
00:08:26,000 --> 00:08:30,379
<font color="#E5E5E5">for your lovers your work is going to</font>

186
00:08:28,399 --> 00:08:32,629
<font color="#E5E5E5">fill</font><font color="#CCCCCC"> a large part of your</font><font color="#E5E5E5"> life</font><font color="#CCCCCC"> and the</font>

187
00:08:30,379 --> 00:08:34,788
only<font color="#E5E5E5"> way</font><font color="#CCCCCC"> to be truly satisfied is to do</font>

188
00:08:32,629 --> 00:08:36,769
what you believe<font color="#CCCCCC"> is great</font><font color="#E5E5E5"> work and the</font>

189
00:08:34,788 --> 00:08:39,679
<font color="#CCCCCC">only way</font><font color="#E5E5E5"> to do great work is to love</font>

190
00:08:36,769 --> 00:08:43,549
<font color="#CCCCCC">what you do if you</font><font color="#E5E5E5"> haven't found it yet</font>

191
00:08:39,679 --> 00:08:45,229
<font color="#CCCCCC">keep looking and</font><font color="#E5E5E5"> don't</font><font color="#CCCCCC"> settle as with</font>

192
00:08:43,549 --> 00:08:47,629
all matters<font color="#E5E5E5"> of the heart you'll know</font>

193
00:08:45,230 --> 00:08:49,430
<font color="#E5E5E5">when you find it</font><font color="#CCCCCC"> and like any</font><font color="#E5E5E5"> great</font>

194
00:08:47,629 --> 00:08:52,129
relationship it<font color="#CCCCCC"> just</font><font color="#E5E5E5"> gets better and</font>

195
00:08:49,429 --> 00:08:55,479
<font color="#CCCCCC">better as the years roll on</font><font color="#E5E5E5"> so keep</font>

196
00:08:52,129 --> 00:08:55,480
<font color="#E5E5E5">looking don't settle</font>

197
00:09:04,049 --> 00:09:11,099
my third story<font color="#E5E5E5"> is about death</font><font color="#CCCCCC"> when I was</font>

198
00:09:09,149 --> 00:09:14,370
17<font color="#CCCCCC"> I read a quote that went something</font>

199
00:09:11,100 --> 00:09:16,470
like<font color="#E5E5E5"> if you live each day as if</font><font color="#CCCCCC"> it was</font>

200
00:09:14,370 --> 00:09:21,810
<font color="#E5E5E5">your last someday you'll most</font><font color="#CCCCCC"> certainly</font>

201
00:09:16,470 --> 00:09:24,690
be<font color="#E5E5E5"> right it made an impression on</font><font color="#CCCCCC"> me and</font>

202
00:09:21,809 --> 00:09:26,609
<font color="#E5E5E5">since</font><font color="#CCCCCC"> then for</font><font color="#E5E5E5"> the past 33 years</font><font color="#CCCCCC"> I've</font>

203
00:09:24,690 --> 00:09:29,070
<font color="#E5E5E5">looked in the mirror every</font><font color="#CCCCCC"> morning</font><font color="#E5E5E5"> and</font>

204
00:09:26,610 --> 00:09:29,970
asked myself if today<font color="#E5E5E5"> were the last day</font>

205
00:09:29,070 --> 00:09:32,278
of my life

206
00:09:29,970 --> 00:09:34,860
what I want to do what I<font color="#E5E5E5"> am about to do</font>

207
00:09:32,278 --> 00:09:37,708
<font color="#CCCCCC">today</font><font color="#E5E5E5"> and whenever the</font><font color="#CCCCCC"> answer has been</font>

208
00:09:34,860 --> 00:09:40,560
no for too many days<font color="#CCCCCC"> in a row I know I</font>

209
00:09:37,708 --> 00:09:42,750
need to<font color="#E5E5E5"> change</font><font color="#CCCCCC"> something</font><font color="#E5E5E5"> remembering</font>

210
00:09:40,559 --> 00:09:44,489
<font color="#E5E5E5">that I'll be dead soon</font><font color="#CCCCCC"> is the most</font>

211
00:09:42,750 --> 00:09:46,610
important tool I've ever<font color="#CCCCCC"> encountered to</font>

212
00:09:44,490 --> 00:09:49,339
<font color="#CCCCCC">help me make the big choices</font><font color="#E5E5E5"> in life</font>

213
00:09:46,610 --> 00:09:52,680
<font color="#CCCCCC">because</font><font color="#E5E5E5"> almost everything all external</font>

214
00:09:49,339 --> 00:09:54,750
expectations all pride all<font color="#CCCCCC"> fear of</font>

215
00:09:52,679 --> 00:09:56,789
<font color="#E5E5E5">embarrassment or failure</font><font color="#CCCCCC"> these things</font>

216
00:09:54,750 --> 00:09:59,429
<font color="#E5E5E5">just</font><font color="#CCCCCC"> fall away in the face of death</font>

217
00:09:56,789 --> 00:10:02,250
leaving<font color="#E5E5E5"> only what is truly important</font>

218
00:09:59,429 --> 00:10:05,069
remembering that you<font color="#CCCCCC"> are</font><font color="#E5E5E5"> going to die is</font>

219
00:10:02,250 --> 00:10:07,740
the<font color="#CCCCCC"> best way I know to avoid</font><font color="#E5E5E5"> the trap of</font>

220
00:10:05,070 --> 00:10:10,500
thinking you<font color="#CCCCCC"> have something to lose you</font>

221
00:10:07,740 --> 00:10:14,459
<font color="#CCCCCC">are already naked there is no</font><font color="#E5E5E5"> reason not</font>

222
00:10:10,500 --> 00:10:18,089
<font color="#E5E5E5">to</font><font color="#CCCCCC"> follow your heart</font><font color="#E5E5E5"> about</font><font color="#CCCCCC"> a year</font><font color="#E5E5E5"> ago I</font>

223
00:10:14,458 --> 00:10:20,189
was diagnosed with<font color="#E5E5E5"> cancer</font><font color="#CCCCCC"> I had a scan</font>

224
00:10:18,089 --> 00:10:23,279
at 7:30 in<font color="#CCCCCC"> the morning</font><font color="#E5E5E5"> and it clearly</font>

225
00:10:20,190 --> 00:10:25,770
showed a tumor<font color="#E5E5E5"> on my pancreas</font><font color="#CCCCCC"> I didn't</font>

226
00:10:23,278 --> 00:10:27,659
<font color="#E5E5E5">even know what a pancreas was the</font>

227
00:10:25,769 --> 00:10:29,490
doctors told me this was almost

228
00:10:27,659 --> 00:10:31,588
<font color="#E5E5E5">certainly</font><font color="#CCCCCC"> a type</font><font color="#E5E5E5"> of cancer</font><font color="#CCCCCC"> that is</font>

229
00:10:29,490 --> 00:10:34,339
<font color="#CCCCCC">incurable and that I should</font><font color="#E5E5E5"> expect to</font>

230
00:10:31,589 --> 00:10:37,649
live no<font color="#CCCCCC"> longer</font><font color="#E5E5E5"> than three to</font><font color="#CCCCCC"> six months</font>

231
00:10:34,339 --> 00:10:40,050
my doctor advised me<font color="#CCCCCC"> to go home</font><font color="#E5E5E5"> and get</font>

232
00:10:37,649 --> 00:10:44,070
<font color="#CCCCCC">my affairs in</font><font color="#E5E5E5"> order which is doctors</font>

233
00:10:40,049 --> 00:10:46,069
code for prepare to die<font color="#E5E5E5"> it means to try</font>

234
00:10:44,070 --> 00:10:48,510
and<font color="#CCCCCC"> tell your kids everything you</font>

235
00:10:46,070 --> 00:10:51,600
<font color="#E5E5E5">thought</font><font color="#CCCCCC"> you'd have</font><font color="#E5E5E5"> the next</font><font color="#CCCCCC"> 10 years to</font>

236
00:10:48,509 --> 00:10:53,189
<font color="#CCCCCC">tell them in just</font><font color="#E5E5E5"> a few</font><font color="#CCCCCC"> months it means</font>

237
00:10:51,600 --> 00:10:55,620
to make sure everything is<font color="#E5E5E5"> buttoned up</font>

238
00:10:53,190 --> 00:10:57,870
so<font color="#CCCCCC"> that</font><font color="#E5E5E5"> will be as easy as possible for</font>

239
00:10:55,620 --> 00:10:58,500
your family<font color="#E5E5E5"> it means to say your</font>

240
00:10:57,870 --> 00:11:03,720
<font color="#E5E5E5">goodbyes</font>

241
00:10:58,500 --> 00:11:06,149
I live with that diagnosis all day<font color="#E5E5E5"> later</font>

242
00:11:03,720 --> 00:11:07,680
<font color="#CCCCCC">that evening I had a biopsy where they</font>

243
00:11:06,149 --> 00:11:09,480
stuck an endoscope down my throat

244
00:11:07,679 --> 00:11:12,239
<font color="#E5E5E5">through my stomach</font><font color="#CCCCCC"> and into my</font>

245
00:11:09,480 --> 00:11:15,089
<font color="#CCCCCC">intestines</font><font color="#E5E5E5"> put a needle into my pancreas</font>

246
00:11:12,240 --> 00:11:17,879
<font color="#E5E5E5">and got a few</font><font color="#CCCCCC"> cells from the tumor I was</font>

247
00:11:15,089 --> 00:11:19,589
sedated<font color="#E5E5E5"> but my wife who was there</font>

248
00:11:17,879 --> 00:11:21,838
told me<font color="#E5E5E5"> that when they viewed the cells</font>

249
00:11:19,589 --> 00:11:23,850
<font color="#E5E5E5">under a microscope</font><font color="#CCCCCC"> the doctor</font><font color="#E5E5E5"> started</font>

250
00:11:21,839 --> 00:11:26,130
crying<font color="#E5E5E5"> because</font><font color="#CCCCCC"> it turned out to be a</font>

251
00:11:23,850 --> 00:11:28,740
very rare form<font color="#E5E5E5"> of pancreatic</font><font color="#CCCCCC"> cancer that</font>

252
00:11:26,129 --> 00:11:40,289
<font color="#E5E5E5">is curable with surgery</font><font color="#CCCCCC"> I had the</font>

253
00:11:28,740 --> 00:11:42,060
<font color="#CCCCCC">surgery</font><font color="#E5E5E5"> and thankfully I'm fine now this</font>

254
00:11:40,289 --> 00:11:44,099
was the closest I've been to facing

255
00:11:42,059 --> 00:11:46,649
death and I hope it's the closest I get

256
00:11:44,100 --> 00:11:48,899
<font color="#E5E5E5">for a few more decades having lived</font>

257
00:11:46,649 --> 00:11:50,519
through<font color="#E5E5E5"> it</font><font color="#CCCCCC"> I can now say this to you</font>

258
00:11:48,899 --> 00:11:52,139
<font color="#E5E5E5">with a bit more certainty</font><font color="#CCCCCC"> than when</font>

259
00:11:50,519 --> 00:11:56,539
death<font color="#E5E5E5"> was a useful but purely</font>

260
00:11:52,139 --> 00:11:58,919
intellectual concept no one wants to die

261
00:11:56,539 --> 00:12:01,980
<font color="#E5E5E5">even</font><font color="#CCCCCC"> people who want to go</font><font color="#E5E5E5"> to heaven</font>

262
00:11:58,919 --> 00:12:04,948
<font color="#CCCCCC">don't want to die to get</font><font color="#E5E5E5"> there</font><font color="#CCCCCC"> and yet</font>

263
00:12:01,980 --> 00:12:08,250
death is<font color="#E5E5E5"> the destination</font><font color="#CCCCCC"> we all share no</font>

264
00:12:04,948 --> 00:12:11,039
<font color="#CCCCCC">one has</font><font color="#E5E5E5"> ever escaped it and that is as</font>

265
00:12:08,250 --> 00:12:13,490
<font color="#E5E5E5">it should be</font><font color="#CCCCCC"> because death is very</font>

266
00:12:11,039 --> 00:12:16,559
<font color="#CCCCCC">likely</font><font color="#E5E5E5"> the</font><font color="#CCCCCC"> single best invention of life</font>

267
00:12:13,490 --> 00:12:19,318
<font color="#CCCCCC">it's life's change agent</font><font color="#E5E5E5"> it clears out</font>

268
00:12:16,559 --> 00:12:19,859
the<font color="#E5E5E5"> old to make way</font><font color="#CCCCCC"> for the new</font><font color="#E5E5E5"> right</font>

269
00:12:19,318 --> 00:12:23,099
<font color="#CCCCCC">now</font>

270
00:12:19,860 --> 00:12:25,079
the new<font color="#CCCCCC"> is you but some day not too</font><font color="#E5E5E5"> long</font>

271
00:12:23,100 --> 00:12:28,439
<font color="#CCCCCC">from now</font><font color="#E5E5E5"> you will gradually become the</font>

272
00:12:25,078 --> 00:12:32,399
old and be cleared away<font color="#E5E5E5"> sorry to be so</font>

273
00:12:28,438 --> 00:12:34,649
<font color="#CCCCCC">dramatic but it's quite true your time</font>

274
00:12:32,399 --> 00:12:36,539
is<font color="#E5E5E5"> limited so don't waste it living</font>

275
00:12:34,649 --> 00:12:39,208
<font color="#CCCCCC">someone else's life</font>

276
00:12:36,539 --> 00:12:40,588
don't be trapped by<font color="#CCCCCC"> Dogma which is</font>

277
00:12:39,208 --> 00:12:43,438
living with<font color="#CCCCCC"> the results of other</font>

278
00:12:40,589 --> 00:12:45,149
<font color="#CCCCCC">people's thinking don't let the noise of</font>

279
00:12:43,438 --> 00:12:48,059
others opinions drown<font color="#E5E5E5"> out your own inner</font>

280
00:12:45,149 --> 00:12:49,620
voice and<font color="#CCCCCC"> most important have the</font>

281
00:12:48,059 --> 00:12:52,229
courage<font color="#CCCCCC"> to follow</font><font color="#E5E5E5"> your heart</font><font color="#CCCCCC"> and</font>

282
00:12:49,620 --> 00:12:55,740
<font color="#CCCCCC">intuition they somehow</font><font color="#E5E5E5"> already know what</font>

283
00:12:52,230 --> 00:12:59,028
you truly<font color="#CCCCCC"> want to become everything else</font>

284
00:12:55,740 --> 00:12:59,028
is secondary

285
00:13:08,049 --> 00:13:13,328
when I was young<font color="#E5E5E5"> there was an</font><font color="#CCCCCC"> amazing</font>

286
00:13:11,470 --> 00:13:15,910
<font color="#CCCCCC">publication called the</font><font color="#E5E5E5"> Whole Earth</font>

287
00:13:13,328 --> 00:13:18,609
Catalog<font color="#CCCCCC"> which was one</font><font color="#E5E5E5"> of the Bible's of</font>

288
00:13:15,909 --> 00:13:21,068
my generation<font color="#E5E5E5"> it was created by a fellow</font>

289
00:13:18,610 --> 00:13:23,318
named Stuart<font color="#E5E5E5"> brand not far from here in</font>

290
00:13:21,068 --> 00:13:25,958
Menlo<font color="#CCCCCC"> Park</font><font color="#E5E5E5"> and he brought it</font><font color="#CCCCCC"> to life</font>

291
00:13:23,318 --> 00:13:28,238
<font color="#CCCCCC">with his poetic touch</font><font color="#E5E5E5"> this was in the</font>

292
00:13:25,958 --> 00:13:30,578
<font color="#E5E5E5">late 60s before</font><font color="#CCCCCC"> personal computers and</font>

293
00:13:28,239 --> 00:13:32,499
desktop<font color="#E5E5E5"> publishing so it was all made</font>

294
00:13:30,578 --> 00:13:35,169
with typewriters scissors and Polaroid

295
00:13:32,499 --> 00:13:37,808
cameras<font color="#E5E5E5"> it was sort of like Google and</font>

296
00:13:35,169 --> 00:13:41,409
paperback form<font color="#E5E5E5"> 35 years before Google</font>

297
00:13:37,808 --> 00:13:44,980
came along it<font color="#E5E5E5"> was idealistic overflowing</font>

298
00:13:41,409 --> 00:13:46,719
with neat tools and great notions<font color="#E5E5E5"> Stuart</font>

299
00:13:44,980 --> 00:13:49,329
and his<font color="#E5E5E5"> team put out several</font><font color="#CCCCCC"> issues of</font>

300
00:13:46,720 --> 00:13:51,610
the Whole Earth Catalog<font color="#CCCCCC"> and then when it</font>

301
00:13:49,328 --> 00:13:51,878
had run its course<font color="#E5E5E5"> they put out a final</font>

302
00:13:51,610 --> 00:13:55,749
<font color="#E5E5E5">issue</font>

303
00:13:51,879 --> 00:13:59,499
<font color="#CCCCCC">it was the mid-1970s and I was your age</font>

304
00:13:55,749 --> 00:14:01,959
<font color="#CCCCCC">on the back</font><font color="#E5E5E5"> cover of their final issue</font>

305
00:13:59,499 --> 00:14:04,420
<font color="#E5E5E5">was a</font><font color="#CCCCCC"> photograph of an early-morning</font>

306
00:14:01,958 --> 00:14:06,458
country road<font color="#E5E5E5"> the kind you might find</font>

307
00:14:04,419 --> 00:14:09,389
yourself hitchhiking on if you were so

308
00:14:06,458 --> 00:14:13,479
adventurous<font color="#E5E5E5"> beneath it were the words</font>

309
00:14:09,389 --> 00:14:16,568
<font color="#E5E5E5">stay hungry stay</font><font color="#CCCCCC"> foolish it was their</font>

310
00:14:13,480 --> 00:14:19,928
farewell<font color="#E5E5E5"> message as they signed off stay</font>

311
00:14:16,568 --> 00:14:24,159
hungry stay<font color="#E5E5E5"> foolish and I have always</font>

312
00:14:19,928 --> 00:14:26,948
wished<font color="#E5E5E5"> that for</font><font color="#CCCCCC"> myself</font><font color="#E5E5E5"> and now as you</font>

313
00:14:24,159 --> 00:14:31,419
graduate<font color="#CCCCCC"> to begin anew I wish that</font><font color="#E5E5E5"> for</font>

314
00:14:26,948 --> 00:14:33,868
<font color="#CCCCCC">you</font><font color="#E5E5E5"> stay hungry stay foolish thank you</font>

315
00:14:31,419 --> 00:14:33,868
all very much

316
00:14:37,070 --> 00:14:39,129
you

317
00:14:50,039 --> 00:14:52,099
<font color="#CCCCCC">you</font>

318
00:14:55,820 --> 00:15:01,160
the preceding<font color="#E5E5E5"> program is copyrighted by</font>

319
00:14:58,429 --> 00:15:03,639
Stanford University<font color="#CCCCCC"> please visit</font><font color="#E5E5E5"> us at</font>

320
00:15:01,159 --> 00:15:03,639
stanford.edu

