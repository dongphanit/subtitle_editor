
import pysrt
import json
from glob import glob
from pydub import AudioSegment
import urllib2 as urlreq # Python 2.x
from time import sleep
from googletrans import Translator
import subtitle
from cloud_client import snippets
import sys
reload(sys)
sys.setdefaultencoding('utf8')

rootAudio = None
SPEED_LEVEL_0 = 4.8
SPEED_LEVEL_1 = 5.2
SPEED_LEVEL_2 = 5.7
SPEED_LEVEL_3 = 6.66
totalSentenes = 0
countProcess = 0
currentIndexAPIKey = 0
listAPIKey = ['7021d1d5ee3946edae2e52ea35d905d8', '0c39d74211f042c2ae615a157038421c', 'd8e7c2afa54a4785af68ed2a4aae269b', '74964de7fe634c079486ffc749da1fba']

def textToSpeech(video_id, strText, timeSecondStart, timeSecondEnd):
    global currentIndexAPIKey
    if len (strText) == 0:
        return
    speed_leave_request = SPEED_LEVEL_0
#    print(strText)
    listWords = strText.split()
    countPoints = strText.count('.')
    countComma = strText.count(',')
    countWordOfOneSecond = len(listWords)/ ((timeSecondEnd - timeSecondStart) - (countPoints * 0.66) - (countComma * 0.33))
    print('timeSecondStart: {} timeSecondEnd: {}'.format(timeSecondStart,timeSecondEnd ))
    print('countWorkOfOneSecond: {}'.format(len(listWords)))
    
    if countWordOfOneSecond <= SPEED_LEVEL_0:
        speed_leave_request = 0
    elif countWordOfOneSecond <= SPEED_LEVEL_1:
        speed_leave_request = 1
    elif countWordOfOneSecond <= SPEED_LEVEL_2:
        speed_leave_request = 2
    else:
        speed_leave_request = 3
    
    # (url, access_token, api_token) = self.get_api_conf()
    api_url = 'http://api.openfpt.vn/text2speech/v4'
    data = strText.strip()
    request = urlreq.Request(api_url)
    request.data = data
    print('speed_leave_request: {}'.format(speed_leave_request))
    method = ("POST", "GET")
    request.get_method = lambda: method[0]
    request.add_header('api_key', listAPIKey[currentIndexAPIKey])
    request.add_header('voice', 'hatieumai')
    request.add_header('speed', str(speed_leave_request))
    print(request.data)
    d = None
    try:
        dataRes = urlreq.urlopen(request)
        response = dataRes.read()
        d = json.loads(response)
        print(d)

    except Exception, e:
        print(e)
        sleep(2)
        if currentIndexAPIKey < len(listAPIKey):
            currentIndexAPIKey = currentIndexAPIKey + 1
        else:
            currentIndexAPIKey = 0
        textToSpeech(video_id, strText, timeSecondStart, timeSecondEnd)
        return
    path = 'data_processing/'+ video_id+'_' + str(timeSecondStart)+'.mp3'
    print(path)
    download_file(video_id, d['async'], path, timeSecondStart, timeSecondEnd)

def download_file(video_id, url , target, timeSecondStart, timeSecondEnd):
    try:
        src = urlreq.urlopen(url)
        data = src.read()
        dst = open(target, "wb")
        dst.write(data)
        if src:
            src.close()
        if dst:
            dst.close()
        overlayAudio(video_id, timeSecondStart, timeSecondEnd)
    except :
        sleep(2)
        download_file(video_id, url, target, timeSecondStart, timeSecondEnd)
    finally:
        print('done')

def overlayAudio(video_id, timeSeconds, timeSecondEnd):
    global countProcess
    global rootAudio
    countProcess = countProcess + 1
    
    audio = AudioSegment.from_mp3('data_processing/'+ video_id+'_' + str(timeSeconds)+'.mp3')
    if(rootAudio == None):
        rootAudio = AudioSegment.silent(duration=(timeSeconds *1000))
    if rootAudio.duration_seconds < timeSeconds:
        rootAudio = rootAudio + AudioSegment.silent(duration=((timeSeconds -rootAudio.duration_seconds) *1000))
#    oldAudio = audio = AudioSegment.from_mp3('test'+'.mp3')

    rootAudio =  rootAudio + audio;
#    rootAudio = rootAudio.overlay(oldAudio, 0)
#    rootAudio = rootAudio.overlay(audio, timeSeconds *1000)
#    out_f = open("test.mp3" , 'wb')
#    rootAudio.export(out_f, format='mp3')

def cal_subtitle_groups(video_id, sub_path):
    subs = pysrt.open(sub_path)
    lastestTime = 0
    strOutput = ''
    fistTimeSecondOfGroup = None
    global countProcess
    global totalSentenes
    totalSentenes = 0
    countProcess = 0
    
    for sub in subs:
        if fistTimeSecondOfGroup == None:
            fistTimeSecondOfGroup = sub.start.seconds + (sub.start.minutes * 60)
        textLine = sub.text
        if ((sub.start.seconds + (sub.start.minutes * 60))- lastestTime) > 0.5:
            print(fistTimeSecondOfGroup)
            print(strOutput)
            totalSentenes = totalSentenes + 1
            textToSpeech(video_id, strOutput, fistTimeSecondOfGroup, lastestTime)
            strOutput = ''
            fistTimeSecondOfGroup = sub.start.seconds + (sub.start.minutes * 60)
                
        strOutput = strOutput + textLine
        lastestTime = sub.end.seconds + (sub.end.minutes * 60)
#    print('totalSentenes {} countProcess {}'.format(str(totalSentenes), str(countProcess)) )
    path_audio = 'data/audio/'+video_id +'_vi.mp3'
    out_f = open('../'+ path_audio , 'wb')
    rootAudio.export(out_f, format='mp3')
    snippets.upload_blob('thuyetminh','../' + path_audio, path_audio)
    url_audio = snippets.make_blob_public('thuyetminh', path_audio)
    return url_audio

def convertToAudio(video_id, sub_path):
    subs = pysrt.open(sub_path)
    lastestTime = 0
    strOutput = ''
    fistTimeSecondOfGroup = None
    global countProcess
    global totalSentenes
    totalSentenes = 0
    countProcess = 0
    for sub in subs:
        if fistTimeSecondOfGroup == None:
            fistTimeSecondOfGroup = sub.start.seconds + (sub.start.minutes * 60)
        textLine = sub.text
        fistTimeSecondOfGroup = sub.start.seconds + (sub.start.minutes * 60)
        lastestTime = sub.end.seconds + (sub.end.minutes * 60)
        textToSpeech(video_id, textLine, fistTimeSecondOfGroup, lastestTime)
    
    print('totalSentenes {} countProcess {}'.format(str(totalSentenes), str(countProcess)) )
    path_audio = 'data/audio/'+video_id +'_vi.mp3'
    out_f = open('../'+ path_audio , 'wb')
    rootAudio.export(out_f, format='mp3')
    snippets.upload_blob('thuyetminh','../' + path_audio, path_audio)
    url_audio = snippets.make_blob_public('thuyetminh', path_audio)
    return url_audio

if __name__ == '__main__':
    convertToAudio('UF8uR6Z6KLc', 'data/sub/vi/UF8uR6Z6KLc_vi.srt')