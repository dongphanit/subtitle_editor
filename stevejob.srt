1
00:00:06,700 --> 00:00:08,770
this program is brought to you by

2
00:00:08,770 --> 00:00:11,410
Stanford University please visit us at

3
00:00:11,410 --> 00:00:13,889
stanford.edu

4
00:00:20,430 --> 00:00:28,810
thank you I'm honored to be with you

5
00:00:28,810 --> 00:00:30,340
today for your commencement from one of

6
00:00:30,340 --> 00:00:33,780
the finest universities in the world

7
00:00:35,129 --> 00:00:39,550
truth be told I never graduated from

8
00:00:39,550 --> 00:00:43,420
college and this is the closest I've

9
00:00:43,420 --> 00:00:46,620
ever gotten to a college graduation

10
00:00:47,220 --> 00:00:49,809
today I want to tell you three stories

11
00:00:49,809 --> 00:00:52,660
from my life that's it no big deal

12
00:00:52,660 --> 00:00:56,710
just three stories the first story is

13
00:00:56,710 --> 00:01:01,360
about connecting the dots I dropped out

14
00:01:01,360 --> 00:01:03,250
of Reed College after the first six

15
00:01:03,250 --> 00:01:04,899
months but then stayed around as a

16
00:01:04,899 --> 00:01:06,850
drop-in for another 18 months or so

17
00:01:06,850 --> 00:01:10,000
before I really quit so why did I drop

18
00:01:10,000 --> 00:01:14,970
out it started before I was born my

19
00:01:14,970 --> 00:01:17,350
biological mother was a young unwed

20
00:01:17,350 --> 00:01:20,080
graduate student and she decided to put

21
00:01:20,080 --> 00:01:22,959
me up for adoption she felt very

22
00:01:22,959 --> 00:01:24,640
strongly that I should be adopted by

23
00:01:24,640 --> 00:01:26,830
college graduates so everything was all

24
00:01:26,830 --> 00:01:29,170
set for me to be adopted at Birth by a

25
00:01:29,170 --> 00:01:32,380
lawyer and his wife except that when I

26
00:01:32,380 --> 00:01:34,420
popped out they decided at the last

27
00:01:34,420 --> 00:01:37,050
minute that they really wanted a girl so

28
00:01:37,050 --> 00:01:39,459
my parents who were on a waiting list

29
00:01:39,459 --> 00:01:41,410
got a call in the middle of the night

30
00:01:41,410 --> 00:01:44,890
asking we've got an unexpected baby boy

31
00:01:44,890 --> 00:01:50,920
do you want him they said of course my

32
00:01:50,920 --> 00:01:52,869
biological mother found out later that

33
00:01:52,869 --> 00:01:54,640
my mother had never graduated from

34
00:01:54,640 --> 00:01:56,770
college and that my father had never

35
00:01:56,770 --> 00:01:59,649
graduated from high school she refused

36
00:01:59,649 --> 00:02:03,399
to sign the final adoption papers she

37
00:02:03,399 --> 00:02:05,289
only relented a few months later when my

38
00:02:05,289 --> 00:02:07,630
parents promised that I would go to

39
00:02:07,630 --> 00:02:11,200
college this was the start in my life

40
00:02:11,200 --> 00:02:16,530
and 17 years later I did go to college

41
00:02:16,530 --> 00:02:19,390
but I naively chose a college that was

42
00:02:19,390 --> 00:02:22,689
almost as expensive as Stanford and all

43
00:02:22,689 --> 00:02:24,700
of my working-class parents savings were

44
00:02:24,700 --> 00:02:27,700
being spent on my college tuition after

45
00:02:27,700 --> 00:02:28,569
six months

46
00:02:28,569 --> 00:02:31,030
I couldn't see the value in it I had no

47
00:02:31,030 --> 00:02:32,590
idea what I wanted to do with my life

48
00:02:32,590 --> 00:02:35,080
and no idea how college was going to

49
00:02:35,080 --> 00:02:37,390
help me figure it out and here I was

50
00:02:37,390 --> 00:02:39,280
spending all the money my parents had

51
00:02:39,280 --> 00:02:43,209
saved their entire life so I decided to

52
00:02:43,209 --> 00:02:45,340
drop out and trust that it would all

53
00:02:45,340 --> 00:02:48,519
work out okay it was pretty scary at the

54
00:02:48,519 --> 00:02:50,709
time but looking back it was one of the

55
00:02:50,709 --> 00:02:54,370
best decisions I ever made the minute I

56
00:02:54,370 --> 00:02:56,920
dropped out I could stop taking the

57
00:02:56,920 --> 00:02:58,810
required classes that didn't interest me

58
00:02:58,810 --> 00:03:01,630
and begin dropping in on the ones that

59
00:03:01,630 --> 00:03:05,080
looked far more interesting it wasn't

60
00:03:05,080 --> 00:03:07,209
all romantic I didn't have a dorm room

61
00:03:07,209 --> 00:03:09,370
so I slept on the floor in friends rooms

62
00:03:09,370 --> 00:03:12,340
I returned coke bottles for the 5 cent

63
00:03:12,340 --> 00:03:14,860
deposits to buy food with and I would

64
00:03:14,860 --> 00:03:16,510
walk the seven miles across town every

65
00:03:16,510 --> 00:03:19,450
Sunday night to get one good meal a week

66
00:03:19,450 --> 00:03:22,660
at the Hari Krishna temple I loved it

67
00:03:22,660 --> 00:03:25,209
and much of what I stumbled into by

68
00:03:25,209 --> 00:03:27,190
following my curiosity and intuition

69
00:03:27,190 --> 00:03:30,190
turned out to be priceless later on let

70
00:03:30,190 --> 00:03:33,880
me give you one example Reid college at

71
00:03:33,880 --> 00:03:35,650
that time offered perhaps the best

72
00:03:35,650 --> 00:03:37,480
calligraphy instruction in the country

73
00:03:37,480 --> 00:03:40,989
throughout the campus every poster every

74
00:03:40,989 --> 00:03:42,790
label on every drawer was beautifully

75
00:03:42,790 --> 00:03:45,940
hand calligraphed because I had dropped

76
00:03:45,940 --> 00:03:47,739
out and didn't have to take the normal

77
00:03:47,739 --> 00:03:50,590
classes I decided to take a calligraphy

78
00:03:50,590 --> 00:03:53,230
class to learn how to do this I learned

79
00:03:53,230 --> 00:03:55,359
about serif and sans-serif typefaces

80
00:03:55,359 --> 00:03:57,370
about varying the amount of space

81
00:03:57,370 --> 00:03:59,139
between different letter combinations

82
00:03:59,139 --> 00:04:02,250
about what makes great typography great

83
00:04:02,250 --> 00:04:05,549
it was beautiful historical artistically

84
00:04:05,549 --> 00:04:08,049
subtle in a way that science can't

85
00:04:08,049 --> 00:04:12,099
capture and I found it fascinating none

86
00:04:12,099 --> 00:04:14,099
of this had even a hope of any practical

87
00:04:14,099 --> 00:04:17,918
application in my life but 10 years

88
00:04:17,918 --> 00:04:19,930
later when we were designing the first

89
00:04:19,930 --> 00:04:22,360
Macintosh computer it all came back to

90
00:04:22,360 --> 00:04:25,150
me and we designed it all into the Mac

91
00:04:25,150 --> 00:04:27,099
it was the first computer with beautiful

92
00:04:27,099 --> 00:04:30,550
typography if I had never dropped in on

93
00:04:30,550 --> 00:04:32,979
that single course in college the Mac

94
00:04:32,979 --> 00:04:34,479
would have never had multiple typefaces

95
00:04:34,479 --> 00:04:37,510
or proportionally spaced fonts and since

96
00:04:37,510 --> 00:04:39,849
windows just copied the Mac it's likely

97
00:04:39,849 --> 00:04:41,230
that no personal computer

98
00:04:41,230 --> 00:04:51,160
have them if I had never dropped out I

99
00:04:51,160 --> 00:04:52,600
would have never dropped in on that

100
00:04:52,600 --> 00:04:54,850
calligraphy class and personal computers

101
00:04:54,850 --> 00:04:56,260
might not have the wonderful typography

102
00:04:56,260 --> 00:04:59,110
that they do of course it was impossible

103
00:04:59,110 --> 00:05:01,240
to connect the dots looking forward when

104
00:05:01,240 --> 00:05:03,730
I was in college but it was very very

105
00:05:03,730 --> 00:05:05,950
clear looking backwards ten years later

106
00:05:05,950 --> 00:05:09,010
again you can't connect the dots looking

107
00:05:09,010 --> 00:05:10,780
forward you can only connect them

108
00:05:10,780 --> 00:05:13,570
looking backwards so you have to trust

109
00:05:13,570 --> 00:05:15,310
that the dots will somehow connect in

110
00:05:15,310 --> 00:05:17,200
your future you have to trust in

111
00:05:17,200 --> 00:05:19,690
something your gut destiny life karma

112
00:05:19,690 --> 00:05:22,570
whatever because believing that the dots

113
00:05:22,570 --> 00:05:25,360
will connect down the road will give you

114
00:05:25,360 --> 00:05:27,280
the confidence to follow your heart even

115
00:05:27,280 --> 00:05:29,830
when it leads you off the well-worn path

116
00:05:29,830 --> 00:05:38,440
and that will make all the difference my

117
00:05:38,440 --> 00:05:42,960
second story is about love and loss I

118
00:05:42,960 --> 00:05:46,240
was lucky I found what I loved to do

119
00:05:46,240 --> 00:05:49,270
early in life woz and I started Apple in

120
00:05:49,270 --> 00:05:51,670
my parent's garage when I was 20 we

121
00:05:51,670 --> 00:05:53,890
worked hard and in 10 years Apple had

122
00:05:53,890 --> 00:05:55,240
grown from just the two of us in a

123
00:05:55,240 --> 00:05:57,640
garage into a two billion dollar company

124
00:05:57,640 --> 00:06:00,280
with over 4,000 employees we just

125
00:06:00,280 --> 00:06:01,990
released our finest creation the

126
00:06:01,990 --> 00:06:04,030
Macintosh a year earlier and I just

127
00:06:04,030 --> 00:06:09,040
turned 30 and then I got fired how can

128
00:06:09,040 --> 00:06:11,250
you get fired from a company you started

129
00:06:11,250 --> 00:06:15,100
well as Apple grew we hired someone who

130
00:06:15,100 --> 00:06:17,170
I thought was very talented to run the

131
00:06:17,170 --> 00:06:18,940
company with me and for the first year

132
00:06:18,940 --> 00:06:21,100
or so things went well but then our

133
00:06:21,100 --> 00:06:22,750
visions of the future began to diverge

134
00:06:22,750 --> 00:06:25,510
and eventually we had a falling out when

135
00:06:25,510 --> 00:06:27,670
we did our Board of Directors sided with

136
00:06:27,670 --> 00:06:31,300
him and so at 30 I was out and very

137
00:06:31,300 --> 00:06:33,880
publicly out what had been the focus of

138
00:06:33,880 --> 00:06:36,280
my entire adult life was gone and it was

139
00:06:36,280 --> 00:06:39,010
devastating I really didn't know what to

140
00:06:39,010 --> 00:06:41,350
do for a few months I felt that I had

141
00:06:41,350 --> 00:06:42,790
let the previous generation of

142
00:06:42,790 --> 00:06:44,950
entrepreneurs down that I had dropped

143
00:06:44,950 --> 00:06:47,040
the baton as it was being passed to me I

144
00:06:47,040 --> 00:06:49,990
met with David Packard and Bob Noyce and

145
00:06:49,990 --> 00:06:52,480
tried to apologize for screwing up so

146
00:06:52,480 --> 00:06:55,310
badly I was a very public fail

147
00:06:55,310 --> 00:06:56,720
and I even thought about running away

148
00:06:56,720 --> 00:06:58,970
from the valley but something slowly

149
00:06:58,970 --> 00:07:02,570
began to dawn on me I still loved what I

150
00:07:02,570 --> 00:07:06,020
did the turn of events at Apple had not

151
00:07:06,020 --> 00:07:08,450
changed that one bit I've been rejected

152
00:07:08,450 --> 00:07:12,380
but I was still in love and so I decided

153
00:07:12,380 --> 00:07:16,100
to start over I didn't see it then but

154
00:07:16,100 --> 00:07:17,419
it turned out that getting fired from

155
00:07:17,419 --> 00:07:18,710
Apple was the best thing that could have

156
00:07:18,710 --> 00:07:21,169
ever happened to me the heaviness of

157
00:07:21,169 --> 00:07:23,360
being successful was replaced by the

158
00:07:23,360 --> 00:07:25,970
lightness of being a beginner again less

159
00:07:25,970 --> 00:07:28,280
sure about everything it freed me to

160
00:07:28,280 --> 00:07:29,540
enter one of the most creative periods

161
00:07:29,540 --> 00:07:32,210
of my life during the next five years I

162
00:07:32,210 --> 00:07:34,340
started a company named next another

163
00:07:34,340 --> 00:07:36,380
company named Pixar and fell in love

164
00:07:36,380 --> 00:07:37,700
with an amazing woman who would become

165
00:07:37,700 --> 00:07:40,370
my wife Pixar went on to create the

166
00:07:40,370 --> 00:07:42,050
world's first computer animated feature

167
00:07:42,050 --> 00:07:44,600
film Toy Story and is now the most

168
00:07:44,600 --> 00:07:48,520
successful animation studio in the world

169
00:07:48,520 --> 00:07:52,160
in a remarkable turn of events Apple

170
00:07:52,160 --> 00:07:54,470
bought next and I returned to Apple and

171
00:07:54,470 --> 00:07:56,660
the technology we developed it next is

172
00:07:56,660 --> 00:07:58,040
at the heart of Apple's current

173
00:07:58,040 --> 00:08:00,740
Renaissance and Laureen and I have a

174
00:08:00,740 --> 00:08:03,890
wonderful family together I'm pretty

175
00:08:03,890 --> 00:08:05,750
sure none of this would have happened if

176
00:08:05,750 --> 00:08:07,160
I hadn't been fired from Apple

177
00:08:07,160 --> 00:08:09,560
it was awful tasting medicine but I

178
00:08:09,560 --> 00:08:12,410
guess the patient needed it sometime

179
00:08:12,410 --> 00:08:14,300
life sometimes life's going to hit you

180
00:08:14,300 --> 00:08:16,340
in the head with a brick don't lose

181
00:08:16,340 --> 00:08:18,950
faith I'm convinced that the only thing

182
00:08:18,950 --> 00:08:20,750
that kept me going was that I loved what

183
00:08:20,750 --> 00:08:23,479
I did you've got to find what you love

184
00:08:23,479 --> 00:08:26,000
and that is this true for work as it is

185
00:08:26,000 --> 00:08:28,400
for your lovers your work is going to

186
00:08:28,400 --> 00:08:30,380
fill a large part of your life and the

187
00:08:30,380 --> 00:08:32,630
only way to be truly satisfied is to do

188
00:08:32,630 --> 00:08:34,789
what you believe is great work and the

189
00:08:34,789 --> 00:08:36,770
only way to do great work is to love

190
00:08:36,770 --> 00:08:39,679
what you do if you haven't found it yet

191
00:08:39,679 --> 00:08:43,549
keep looking and don't settle as with

192
00:08:43,549 --> 00:08:45,230
all matters of the heart you'll know

193
00:08:45,230 --> 00:08:47,630
when you find it and like any great

194
00:08:47,630 --> 00:08:49,430
relationship it just gets better and

195
00:08:49,430 --> 00:08:52,130
better as the years roll on so keep

196
00:08:52,130 --> 00:08:55,480
looking don't settle

197
00:09:04,050 --> 00:09:09,149
my third story is about death when I was

198
00:09:09,149 --> 00:09:11,100
17 I read a quote that went something

199
00:09:11,100 --> 00:09:14,370
like if you live each day as if it was

200
00:09:14,370 --> 00:09:16,470
your last someday you'll most certainly

201
00:09:16,470 --> 00:09:21,810
be right it made an impression on me and

202
00:09:21,810 --> 00:09:24,690
since then for the past 33 years I've

203
00:09:24,690 --> 00:09:26,610
looked in the mirror every morning and

204
00:09:26,610 --> 00:09:29,070
asked myself if today were the last day

205
00:09:29,070 --> 00:09:29,970
of my life

206
00:09:29,970 --> 00:09:32,279
what I want to do what I am about to do

207
00:09:32,279 --> 00:09:34,860
today and whenever the answer has been

208
00:09:34,860 --> 00:09:37,709
no for too many days in a row I know I

209
00:09:37,709 --> 00:09:40,560
need to change something remembering

210
00:09:40,560 --> 00:09:42,750
that I'll be dead soon is the most

211
00:09:42,750 --> 00:09:44,490
important tool I've ever encountered to

212
00:09:44,490 --> 00:09:46,610
help me make the big choices in life

213
00:09:46,610 --> 00:09:49,339
because almost everything all external

214
00:09:49,339 --> 00:09:52,680
expectations all pride all fear of

215
00:09:52,680 --> 00:09:54,750
embarrassment or failure these things

216
00:09:54,750 --> 00:09:56,790
just fall away in the face of death

217
00:09:56,790 --> 00:09:59,430
leaving only what is truly important

218
00:09:59,430 --> 00:10:02,250
remembering that you are going to die is

219
00:10:02,250 --> 00:10:05,070
the best way I know to avoid the trap of

220
00:10:05,070 --> 00:10:07,740
thinking you have something to lose you

221
00:10:07,740 --> 00:10:10,500
are already naked there is no reason not

222
00:10:10,500 --> 00:10:14,459
to follow your heart about a year ago I

223
00:10:14,459 --> 00:10:18,089
was diagnosed with cancer I had a scan

224
00:10:18,089 --> 00:10:20,190
at 7:30 in the morning and it clearly

225
00:10:20,190 --> 00:10:23,279
showed a tumor on my pancreas I didn't

226
00:10:23,279 --> 00:10:25,770
even know what a pancreas was the

227
00:10:25,770 --> 00:10:27,660
doctors told me this was almost

228
00:10:27,660 --> 00:10:29,490
certainly a type of cancer that is

229
00:10:29,490 --> 00:10:31,589
incurable and that I should expect to

230
00:10:31,589 --> 00:10:34,339
live no longer than three to six months

231
00:10:34,339 --> 00:10:37,649
my doctor advised me to go home and get

232
00:10:37,649 --> 00:10:40,050
my affairs in order which is doctors

233
00:10:40,050 --> 00:10:44,070
code for prepare to die it means to try

234
00:10:44,070 --> 00:10:46,070
and tell your kids everything you

235
00:10:46,070 --> 00:10:48,510
thought you'd have the next 10 years to

236
00:10:48,510 --> 00:10:51,600
tell them in just a few months it means

237
00:10:51,600 --> 00:10:53,190
to make sure everything is buttoned up

238
00:10:53,190 --> 00:10:55,620
so that will be as easy as possible for

239
00:10:55,620 --> 00:10:57,870
your family it means to say your

240
00:10:57,870 --> 00:10:58,500
goodbyes

241
00:10:58,500 --> 00:11:03,720
I live with that diagnosis all day later

242
00:11:03,720 --> 00:11:06,149
that evening I had a biopsy where they

243
00:11:06,149 --> 00:11:07,680
stuck an endoscope down my throat

244
00:11:07,680 --> 00:11:09,480
through my stomach and into my

245
00:11:09,480 --> 00:11:12,240
intestines put a needle into my pancreas

246
00:11:12,240 --> 00:11:15,089
and got a few cells from the tumor I was

247
00:11:15,089 --> 00:11:17,880
sedated but my wife who was there

248
00:11:17,880 --> 00:11:19,589
told me that when they viewed the cells

249
00:11:19,589 --> 00:11:21,839
under a microscope the doctor started

250
00:11:21,839 --> 00:11:23,850
crying because it turned out to be a

251
00:11:23,850 --> 00:11:26,130
very rare form of pancreatic cancer that

252
00:11:26,130 --> 00:11:28,740
is curable with surgery I had the

253
00:11:28,740 --> 00:11:40,290
surgery and thankfully I'm fine now this

254
00:11:40,290 --> 00:11:42,060
was the closest I've been to facing

255
00:11:42,060 --> 00:11:44,100
death and I hope it's the closest I get

256
00:11:44,100 --> 00:11:46,649
for a few more decades having lived

257
00:11:46,649 --> 00:11:48,899
through it I can now say this to you

258
00:11:48,899 --> 00:11:50,519
with a bit more certainty than when

259
00:11:50,519 --> 00:11:52,139
death was a useful but purely

260
00:11:52,139 --> 00:11:56,540
intellectual concept no one wants to die

261
00:11:56,540 --> 00:11:58,920
even people who want to go to heaven

262
00:11:58,920 --> 00:12:01,980
don't want to die to get there and yet

263
00:12:01,980 --> 00:12:04,949
death is the destination we all share no

264
00:12:04,949 --> 00:12:08,250
one has ever escaped it and that is as

265
00:12:08,250 --> 00:12:11,040
it should be because death is very

266
00:12:11,040 --> 00:12:13,490
likely the single best invention of life

267
00:12:13,490 --> 00:12:16,560
it's life's change agent it clears out

268
00:12:16,560 --> 00:12:19,319
the old to make way for the new right

269
00:12:19,319 --> 00:12:19,860
now

270
00:12:19,860 --> 00:12:23,100
the new is you but some day not too long

271
00:12:23,100 --> 00:12:25,079
from now you will gradually become the

272
00:12:25,079 --> 00:12:28,439
old and be cleared away sorry to be so

273
00:12:28,439 --> 00:12:32,399
dramatic but it's quite true your time

274
00:12:32,399 --> 00:12:34,649
is limited so don't waste it living

275
00:12:34,649 --> 00:12:36,540
someone else's life

276
00:12:36,540 --> 00:12:39,209
don't be trapped by Dogma which is

277
00:12:39,209 --> 00:12:40,589
living with the results of other

278
00:12:40,589 --> 00:12:43,439
people's thinking don't let the noise of

279
00:12:43,439 --> 00:12:45,149
others opinions drown out your own inner

280
00:12:45,149 --> 00:12:48,060
voice and most important have the

281
00:12:48,060 --> 00:12:49,620
courage to follow your heart and

282
00:12:49,620 --> 00:12:52,230
intuition they somehow already know what

283
00:12:52,230 --> 00:12:55,740
you truly want to become everything else

284
00:12:55,740 --> 00:12:59,029
is secondary

285
00:13:08,050 --> 00:13:11,470
when I was young there was an amazing

286
00:13:11,470 --> 00:13:13,329
publication called the Whole Earth

287
00:13:13,329 --> 00:13:15,910
Catalog which was one of the Bible's of

288
00:13:15,910 --> 00:13:18,610
my generation it was created by a fellow

289
00:13:18,610 --> 00:13:21,069
named Stuart brand not far from here in

290
00:13:21,069 --> 00:13:23,319
Menlo Park and he brought it to life

291
00:13:23,319 --> 00:13:25,959
with his poetic touch this was in the

292
00:13:25,959 --> 00:13:28,239
late 60s before personal computers and

293
00:13:28,239 --> 00:13:30,579
desktop publishing so it was all made

294
00:13:30,579 --> 00:13:32,499
with typewriters scissors and Polaroid

295
00:13:32,499 --> 00:13:35,170
cameras it was sort of like Google and

296
00:13:35,170 --> 00:13:37,809
paperback form 35 years before Google

297
00:13:37,809 --> 00:13:41,410
came along it was idealistic overflowing

298
00:13:41,410 --> 00:13:44,980
with neat tools and great notions Stuart

299
00:13:44,980 --> 00:13:46,720
and his team put out several issues of

300
00:13:46,720 --> 00:13:49,329
the Whole Earth Catalog and then when it

301
00:13:49,329 --> 00:13:51,610
had run its course they put out a final

302
00:13:51,610 --> 00:13:51,879
issue

303
00:13:51,879 --> 00:13:55,749
it was the mid-1970s and I was your age

304
00:13:55,749 --> 00:13:59,499
on the back cover of their final issue

305
00:13:59,499 --> 00:14:01,959
was a photograph of an early-morning

306
00:14:01,959 --> 00:14:04,420
country road the kind you might find

307
00:14:04,420 --> 00:14:06,459
yourself hitchhiking on if you were so

308
00:14:06,459 --> 00:14:09,389
adventurous beneath it were the words

309
00:14:09,389 --> 00:14:13,480
stay hungry stay foolish it was their

310
00:14:13,480 --> 00:14:16,569
farewell message as they signed off stay

311
00:14:16,569 --> 00:14:19,929
hungry stay foolish and I have always

312
00:14:19,929 --> 00:14:24,160
wished that for myself and now as you

313
00:14:24,160 --> 00:14:26,949
graduate to begin anew I wish that for

314
00:14:26,949 --> 00:14:31,420
you stay hungry stay foolish thank you

315
00:14:31,420 --> 00:14:33,869
all very much

316
00:14:37,070 --> 00:14:39,130
you

317
00:14:50,040 --> 00:14:52,100
you

318
00:14:55,820 --> 00:14:58,430
the preceding program is copyrighted by

319
00:14:58,430 --> 00:15:01,160
Stanford University please visit us at

320
00:15:01,160 --> 00:00:00,000
stanford.edu

